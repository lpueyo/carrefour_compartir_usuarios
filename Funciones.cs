﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.OleDb;
using System.Data;
using Newtonsoft.Json;
using Carrefour.clases;
using System.IO;
using System.Text;
using System.Net.Mail;

namespace Carrefour
{
    class Funciones
    {
        public const string CadenaConexionAnywhere = "Provider=SQLNCLI11.1;Server=tcp:192.168.1.95;Database=SignesPro;Uid=edoc;Pwd=edoc;Encrypt=True;Connection Timeout=3000;TrustServerCertificate=True";
        public const string CadenaConexionAnywhereSinProvider = "Server=tcp:192.168.1.95;Database=SignesPro;Uid=edoc;Pwd=edoc;Encrypt=True;Connection Timeout=3000;TrustServerCertificate=True";
        public static string UsuarioCarrefour = "104";
        static WSPro.Service1 WS = new WSPro.Service1();
        public static string ExisteExpediente(string Dni,string IdUsuario)
        {            
            
            string IdExpediente= WS.CrearNuevoExpedienteZDocs(Dni, IdUsuario, "");
            WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, IdUsuario);
          
            return "";
        }
        public static string DevolverValorMetadato(string CampoBuscado, string Tabla, string IdDocumento, string IdTipoDocumento,string Formato)
        {
            try
            {
                string IdMetadato = DevolverCampoTabla("IdMetadato", "TiposDocumento", " iddoc=" + IdDocumento + " and idTipoDocumento=" + IdTipoDocumento + " and activo='True'");
                string SQL = "select " + CampoBuscado + " from " + Tabla + " where Idmetadato=" + IdMetadato;
                DataTable datos = clases.DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, Funciones.CadenaConexionAnywhereSinProvider);
                string res = "";
                if (datos.Rows.Count > 0)
                {
                   // res = datos.Rows[0]["" + CampoBuscado + ""].ToString();
                    res = datos.Rows[0].ItemArray[0].ToString();
                    //if ((res.Length) > 10 && CampoBuscado.Contains("Fecha"))
                    //    res = res.Substring(6,4) + res.Substring(4, 2)+ res.Substring(0, 2);
                    if (CampoBuscado.Contains("Fecha"))
                    {
                        res = res.Substring(0, 10);
                        switch (Formato)
                        { 
                            case ("yyyymmdd"):
                                res = res.Substring(6, 4) + res.Substring(3, 2) + res.Substring(0, 2);
                            break;
                        default:
                            res = res;
                            break;
                        }
                    }
                }
                return res;
            }
            catch (Exception e)
            {
                return "";
            }
        }
        public static int InsertarCentro(string IdMetadato, string Centro, string Tabla)
        {
            OleDbConnection vCon = new OleDbConnection(Funciones.CadenaConexionAnywhere);

            string SQL = "update " + Tabla + " set Centro='" + Centro + "' where idmetadato=" + IdMetadato;
            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            int resul = vComando.ExecuteNonQuery();
            vCon.Close();
            return resul;

        }
        public static string RellenarCentro(string IdMetaDato, string Matricula, string Centro, string Tabla)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(Funciones.CadenaConexionAnywhere);
                double ULTIMO;
                string CODIGO = "";
                string SQL = "select Centro from " + Tabla + " where idmetadato=" + IdMetaDato  ;
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);


                if ( ds.Tables[0].Rows.Count> 0)
                {
                    if ( ds.Tables[0].Rows[0].ItemArray[0] == System.DBNull.Value )
                        InsertarCentro(IdMetaDato,Centro,Tabla);
                    
                   
                   
                }

                return "";                

            }
            catch (OleDbException   ex)
            {
                return "-1";
            }
        }
        public static string subirFichero(Byte[] Fichero, string NomFichero, string idusuario, string idExpediente, string idempresa,string formato,string FicheroErrores)
        {
            string resultado = "";
            try
            {
                //string Directorio = "/Inetpub/wwwroot/ZDocs-Pro/Volumenes/";
                
                if (Fichero != null)
                {
                    String Fec = DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString() + DateTime.Today.Day.ToString();
                    string Fecha = string.Format("{0:yyyyMMdd}", DateTime.Today);

                    string ObligadaLectura = "False";
                    byte[] kk = null;
                    String[] aux = NomFichero.Split('.');
                   // String formato = aux[aux.Length - 1];
                    string err = "";

                    string idDoc = WS.insertarDocEnBBDD(NomFichero, idusuario, formato, idempresa, "", "");

                    //if (!string.IsNullOrEmpty(TagActual) & TagActual != "undefined")
                    //    Funciones.AsignaTipo(idDoc, TagActual, "Documentos");
                    string Ruta = "00000000";
                    Ruta = Ruta.Substring(0, 8 - idDoc.Length) + idDoc;
                    //string pathString = Directorio + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/";// +Ruta.Substring(6, 2) + "/";
                    string pathString = Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/";// +Ruta.Substring(6, 2) + "/";
                    System.IO.Directory.CreateDirectory(pathString);

                    //File.WriteAllBytes(pathString + Ruta.Substring(6, 2) + "." + formato, Fichero);
                    string resul=WS.SubirFichero(Fichero, pathString, Ruta.Substring(6, 2) + "." + formato);
                    if (resul!="1")                        
                       File.AppendAllText(FicheroErrores, "Error subiendo Fichero: " + Fichero);

                    if (!WS.ExisteDocumentoUsuario(idDoc, idusuario))
                    {
                        err = WS.InsertarDocumentoenDocumentosUsuario(idDoc, idusuario, "Documentos Propios", idusuario, ObligadaLectura, "", "", "", "", idempresa);
                        if (! string.IsNullOrEmpty(err))
                            File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                        err = "";
                    }
                    if (!string.IsNullOrEmpty(idExpediente))
                    {
                        if (!WS.ExisteDocumentoExpediente(idDoc, idExpediente))
                        {
                            err = WS.InsertarDocumentoenExpediente(idDoc, idExpediente);
                            if (!string.IsNullOrEmpty(err))
                                File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Expedientes");
                            err = "";
                        }
                        err=WS.CompartirDocumentoaUsuariosExpediente(idDoc, idExpediente, idusuario, ObligadaLectura, idempresa);
                        if (!string.IsNullOrEmpty(err))
                            File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario Expedientes");
                        err = "";
                    }
                    //if (File.Exists(pathString + Ruta.Substring(6, 2) + "." + formato)) // + "." + formato  pathString + "/" + Fecha.Substring(6, 2) + "." + formato))                    
                    //    return idDoc;
                    //else
                    //{
                    //    return "-1";
                    //}
                    if (string.IsNullOrEmpty(err))
                        return idDoc;
                    else
                    {                        
                        File.AppendAllText(FicheroErrores, "Error subiendo Fichero");
                        return err;
                    }

                }
                return resultado;
            }
            catch (Exception e)
            {
                return "error";
                //string Fichero = "";
                //Trazas t = new Trazas("SubirDocumento", "ZDocs", Convert.ToInt16(idusuario), e.ToString(), e.InnerException + "");
                //return "-1";
            }
        }
        public static string BuscarDato(string Etiqueta, string Fichero)
        {
            try
            {
                string Valor = "";
                string[] Churro = File.ReadAllLines(Fichero);
                foreach (string Linea in Churro)
                {
                    if (Linea.ToUpper().Contains(Etiqueta.ToUpper()))
                    {
                        if (Etiqueta == "Type")
                        {
                            int Pos1 = Linea.ToUpper().IndexOf("TYPE=");
                            Valor = Linea.Substring(Pos1 + 6);
                            Valor = Valor.Substring(0, Valor.Length - 2);
                        }
                        else
                        {
                            if (Etiqueta == "Name")
                            {
                                int Pos1 = Linea.ToUpper().IndexOf("NAME=");
                                Valor = Linea.Substring(Pos1 + 6);
                                Valor = Valor.Substring(0, Valor.IndexOf("Type") - 2);
                                //  Valor = Valor.Substring(0, Valor.Length - 2);
                            }
                            else
                            {
                                int Pos1 = Linea.ToUpper().IndexOf("VALUE=");
                                if (Pos1 != -1)
                                {
                                    Valor = Linea.Substring(Pos1 + 7);
                                    Valor = Valor.Substring(0, Valor.Length - 2);
                                }
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(Valor))
                        return Valor;

                }

                return Valor;
            }
            catch
            {
                return "-1";
            }
        }
        public static string LeerCampos(string Fichero,string IdDocumento,string IdEmpresa,string IdUsuario,string Dni,string Matricula,string Tipo,string FicheroErrores)
        {
            try
            {
                string Valor = "";
               
                string IdTipo = "";


                IdTipo = WS.DevolverIdTipo(Tipo, IdUsuario, "Documentos", IdEmpresa);
                if (!string.IsNullOrEmpty(IdTipo))
                {
                    WS.ActualizarDocumentosTipos(IdTipo, IdDocumento, "", "Documentos");
                    OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                    string NomTabla = "c00000000";
                    NomTabla = NomTabla.Substring(0, 9 - IdTipo.Length) + IdTipo;
                    string SQL = "exec sp_columns [" + NomTabla + "]";

                    OleDbCommand vComando = new OleDbCommand(SQL, vCon);

                    vCon.Open();
                    OleDbDataAdapter da = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    da.SelectCommand = vComando;
                    da.Fill(ds);

                    string IdMetadato = "";
                    bool Primero = true;
                    string error = "";

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (ds.Tables[0].Rows[i].ItemArray[3].ToString().ToUpper() != "IDMETADATO")
                            {
                                Valor = BuscarDato(ds.Tables[0].Rows[i].ItemArray[3].ToString(), Fichero);
                                if (!string.IsNullOrEmpty(Valor))
                                {
                                    if (ds.Tables[0].Rows[i].ItemArray[5].ToString() == "datetime")
                                    {
                                        if (IsDate(Valor))                                        
                                            Valor = Convert.ToDateTime(Valor).ToString().Substring(0,10);                                                                               
                                    }
                                    if (Primero)
                                    {
                                        //if (ds.Tables[0].Rows[i].ItemArray[5].ToString() == "datetime")                                
                                        //    Valor = Valor.Substring(0, 10);

                                        IdMetadato = WS.InsertarLineaCamposNoWeb(NomTabla, ds.Tables[0].Rows[i].ItemArray[3].ToString(), Valor, IdTipo, IdDocumento, "Documentos");
                                        Primero = false;
                                    }
                                    else
                                    {
                                        //if (ds.Tables[0].Rows[i].ItemArray[5].ToString() == "datetime")
                                        //    Valor = Valor.Substring(0, 10);
                                        error = WS.ActualizarCampo(NomTabla, ds.Tables[0].Rows[i].ItemArray[3].ToString(), Valor, " IdMetadato=" + IdMetadato);
                                        if (error != "1")
                                            File.AppendAllText(FicheroErrores, "Error insertando el campo :" + ds.Tables[0].Rows[i].ItemArray[3].ToString() + " del Fichero: " + Fichero);
                                        error = "";
                                       
                                    }
                                    //WS.ActualizarDocumentosTipos(IdTipo, IdDocumento,IdMetadato,  "Documentos");
                                    //string[] mat = "{\\" + ds.Tables[0].Rows[i].ItemArray[3].ToString() + ": \\" + Valor + ",}";                            

                                }
                                Valor = "";
                            }
                        }
                    }
                    //string error=WS.InsertarLineaCamposNoWeb(NomTabla, "Dni", Dni, IdTipo, IdDocumento, "Documentos");
                    if (Primero)
                    {
                        IdMetadato = WS.InsertarLineaCamposNoWeb(NomTabla, "Dni", Dni, IdTipo, IdDocumento, "Documentos");
                        Primero = false;
                    }
                    else
                    {
                        error = WS.ActualizarCampo(NomTabla, "Dni", Dni, " IdMetadato=" + IdMetadato);
                        if (error != "1")
                            File.AppendAllText(FicheroErrores, "Error insertando el campo Dni en la tabla :" + NomTabla + ", idmetadato : " + IdMetadato);
                        error = "";
                    }
                    error = WS.ActualizarCampo(NomTabla, "Matricula", Matricula, " IdMetadato=" + IdMetadato);
                    if (error != "1")
                        File.AppendAllText(FicheroErrores, "Error insertando el campo Matricula en la tabla :" + NomTabla + ", idmetadato : " + IdMetadato);
                    error = "";
                    error = WS.ActualizarCampo(NomTabla, "Fecha de Recepción", DateTime.Now.ToString(), " IdMetadato=" + IdMetadato);
                    if (error != "1")
                        File.AppendAllText(FicheroErrores, "Error insertando el campo Fecha de Recepción en la tabla :" + NomTabla + ", idmetadato : " + IdMetadato);
                }
                return "";
            }
            catch
            {
                return "-1";
            }
                                           
        }

        public static bool IsDate(string sdate)
        {
            DateTime dt;
            bool isDate = true;

            try
            {
                dt = DateTime.Parse(sdate);
            }
            catch
            {
                isDate = false;
            }

            return isDate;
        }
        public static string ValidarCampos(string Fichero, string IdEmpresa, string IdUsuario, string Dni, string Matricula, string Tipo,string FicheroErrores)
        {
            try
            {
                string Valor = "";
                string IdTipo = "";

                //IdTipo = WS.DevolverIdTipo(Tipo, IdUsuario, "Documentos", IdEmpresa);
                if (Tipo == "CV / Solicitud empleo")
                    IdTipo = "105";
                else
                    if (Tipo == "Plan Remuneración Flexible")
                        IdTipo = "36";
                    else
                        if (Tipo == "Bajas-Confirmaciones-Altas por IT")
                            IdTipo = "49";
                        else
                            IdTipo = WS.DevolverIdTipoSimilar(Tipo, IdUsuario, "Documentos", IdEmpresa);

                if (!string.IsNullOrEmpty(IdTipo))
                {
                    OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                    string NomTabla = "c00000000";
                    NomTabla = NomTabla.Substring(0, 9 - IdTipo.Length) + IdTipo;
                    string SQL = "exec sp_columns [" + NomTabla + "]";

                    OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                    vCon.Open();
                    OleDbDataAdapter da = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    da.SelectCommand = vComando;
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            Valor = BuscarDato(ds.Tables[0].Rows[i].ItemArray[3].ToString(), Fichero);
                            if (!string.IsNullOrEmpty(Valor))
                            {
                                if (ds.Tables[0].Rows[i].ItemArray[5].ToString() == "datetime")
                                {
                                    if (IsDate(Valor))
                                    {
                                        Valor = "";
                                    }
                                    else
                                        return "-1";
                                }
                                else
                                    return "";
                            }
                            Valor = "";
                        }
                    }
                    return "";
                }
                else //NO ENCUENTRA EL TIPO DE DOCUMENTO NO SE PUEDE SEGUIR
                {                    
                    File.AppendAllText(FicheroErrores, "Error, no se encuentra el tipo del documento :" + Fichero );
                    return "-1";
                }
            }
            catch
            {
                return "-1";
            }

        }
        public static bool ExisteFicheroPdf(string NombrePdf,string IdUsuario)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string SQL = "Select * from Documentos where activo='True' and idusuario=" + IdUsuario + " and NombreOriginal='" + NombrePdf + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                    return true;
                else
                    return false;

            }
            catch { return false; }
        }
        public static string BorrarDocsSinMetadatosCarrefour(string IdUsuario)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string SQL = "Select * from Documentos where idusuario=" + IdUsuario + " and not exists (select iddocumento from tiposdocumento  ) ";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                DataTable datos = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, CadenaConexionAnywhere);                
                string IdDocumento="";
                if (datos.Rows.Count > 0)
                {
                    IdDocumento= datos.Rows[0]["iddocumento"].ToString();
                    string NomExpediente = datos.Rows[0]["NombreeOriginal"].ToString().Split('_')[1];
                    string IdExpediente = WS.ExisteExpedienteUsuarioPorNombre(IdUsuario, NomExpediente);
                    EliminarExpedienteUsuario(IdExpediente,IdUsuario);
                }
                else
                    return "-1";

                return "";

            }
            catch { return "-1"; }
        }
        public static string ArreglarComparticionUsuarioCarrefour(string IdUsuarioComparte, string IdUsuarioNuevo)
        {
            try {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                vCon.Open();
                string SQL = "";
                SQL = "SELECT 'INSERT INTO ExpedientesUsuarios (idexpediente, idusuario ) values (',idexpediente FROM expedientes exp WHERE idusuario = " + IdUsuarioComparte + " AND NOT EXISTS (SELECT * FROM expedientesusuarios expusu WHERE idusuario = " + IdUsuarioNuevo + " AND Exp.idexpediente = expusu.idexpediente) ";
                SQL += " AND (NOT EXISTS(SELECT idusuario FROM usuarioscentros uc WHERE uc.idusuario = " + IdUsuarioNuevo + ")  OR  exists (SELECT idusuario FROM usuarioscentros uc2 WHERE uc2.idcentro = exp.IdCentro AND uc2.idusuario = " + IdUsuarioNuevo + ")) ";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            SQL = ds.Tables[0].Rows[i].ItemArray[0].ToString() + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "," + IdUsuarioNuevo + ")";
                            vComando = new OleDbCommand(SQL, vCon);
                            int resultado = vComando.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            string Error = e.Message;
                        }
                    }
                }
                ds.Clear();

                SQL = "  SELECT 'INSERT INTO DocumentosUsuarios (iddocumento, idusuario, carpetadocumento, idusuariocomparte, idempresausuariocomparte ) values (',iddocumento ";
                SQL += "from documentos where idusuario = " + IdUsuarioComparte + "  and not  exists (select * from DocumentosUsuarios where idusuario =" + IdUsuarioNuevo + " and Documentos.idDocumento = DocumentosUsuarios.iddocumento) ";
                SQL += " AND (NOT EXISTS (SELECT idusuario FROM usuarioscentros uc WHERE uc.idusuario = " + IdUsuarioNuevo + ")  OR  exists (SELECT idusuario FROM usuarioscentros uc2 WHERE uc2.idcentro = Documentos.IdCentro AND uc2.idusuario = " + IdUsuarioNuevo + ")) ";
                vComando = new OleDbCommand(SQL, vCon);
                DataSet ds2 = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds2);
                if (ds2.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds2.Tables[0].Rows.Count; i++)
                    {
                        SQL = ds2.Tables[0].Rows[i].ItemArray[0].ToString() + ds2.Tables[0].Rows[i].ItemArray[1].ToString() + "," + IdUsuarioNuevo + ",'Documentos Empresa'," + IdUsuarioComparte + ",9)";

                        vComando = new OleDbCommand(SQL, vCon);
                        int resultado = vComando.ExecuteNonQuery();
                    }
                }
                return "1";
            }
            catch (Exception e) {
                return "-1";
            }
        }
        public static string ArreglarComparticionCarrefour(string IdUsuarioComparte, string IdUsuarioNuevo, string IdEmpresa)
        {
            
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                vCon.Open();
                string SQL = "";
                if (string.IsNullOrEmpty(IdUsuarioComparte))
                    IdUsuarioComparte = UsuarioCarrefour;

                if (string.IsNullOrEmpty(IdUsuarioNuevo))
                {                                                                   //es mio este usuario
                    SQL = "Select IDUSUARIO from usuarioempresa where idempresa=" + IdEmpresa + " AND iDUSUARIO<>135 and idusuario<>" + UsuarioCarrefour;
                    OleDbCommand vComando = new OleDbCommand(SQL, vCon);

                    OleDbDataAdapter da = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    da.SelectCommand = vComando;
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            ArreglarComparticionUsuarioCarrefour(IdUsuarioComparte, ds.Tables[0].Rows[i].ItemArray[0].ToString());
                        }
                    }
                }
                else
                    ArreglarComparticionUsuarioCarrefour(IdUsuarioComparte, IdUsuarioNuevo);

                return "1";
                
                        
            }
            catch 
            {
                return "-1";
            }
        }
        public static string ArreglarComparticion(string IdUsuarioComparte, string IdUsuarioNuevo, string IdEmpresa)
        {//NO USSAR
            try
            {
                string Error = "";
                string ObligadaLectura = "False";
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string SQL = "Select * from Expedientes where idusuario=" + IdUsuarioComparte;
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                // DataTable datos = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, CadenaConexionAnywhere);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string IdExpediente = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                        if (string.IsNullOrEmpty (WS.ExisteExpedienteUsuario(IdExpediente,IdUsuarioNuevo)))
                           Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, IdUsuarioNuevo);

                        string SQL2 = "Select * from DocumentosExpedientes where idexpediente=" + IdExpediente;
                        OleDbCommand vComando2 = new OleDbCommand(SQL2, vCon);

                        OleDbDataAdapter da2 = new OleDbDataAdapter();
                        DataSet ds2 = new DataSet();

                        da2.SelectCommand = vComando2;
                        da2.Fill(ds2);

                        //DataTable datos2 = DatabaseConnection.executeNonQueryDT(SQL2, CommandType.Text, CadenaConexionAnywhere);

                        for (int i2 = 0; i2 < ds2.Tables[0].Rows.Count; i2++)
                        {
                            string Iddocumento = ds2.Tables[0].Rows[i2].ItemArray[0].ToString();
                            if (!WS.ExisteDocumentoUsuario(Iddocumento,IdUsuarioNuevo))
                               Error = WS.InsertarDocumentoenDocumentosUsuario(Iddocumento, IdUsuarioNuevo, "Documentos Empresa", IdUsuarioComparte, ObligadaLectura, "", "", "", "", IdEmpresa);
                        }
                    }
                }
                else
                    return "-1";


                return "";
            }
            catch
            {
                return "-1";
            }
        }
        public static string CrearCuerpoCorreoAlta(string Company, string Password)
        {
            string _htmlheader = "<table width='98%' style='width: 98%;'>" +
                                       "<tr>" +
                                         "<td width='100%' style='width: 100%; padding: 30pt;'>" +
                                             "<table style='font-family: arial; font-size: 12px; width: 631px;'>" +
                                                 "<tr><td style='background: none repeat scroll 0% 0% #1f497d; padding-left:25px; height:32px; font-size:23px; font-family: Helvetica;'><p>" +
                                                    "<span style='color: white; letter-spacing: -0.35pt;'>ZERTIFIKA<b><span style='color: white; font-size:9px;'> anywhere</span></b></p>" +
                                                 "</tr>" +
                                                 "<tr>" +
                                                     "<td valign='top' style='border-width: medium 1pt 1pt; border-style: none solid solid; border-color: #385D8A; background: white; padding: 25px;' colspan='2'>";


            string _htmlSignature = "<div style='font-size:13px; padding: 10px 0 0 25px;'>";
            _htmlSignature += "<b><label>" + "ZDocs, su nuevo servicio web de gestión de documentos:";

            _htmlSignature += "</label><a style='color:#005f98; text-decoration: none;' href='http://192.168.1.197/ZDocs'> ZERTIFIKA anywhere</b></a>";

            _htmlSignature += "</div>";

            _htmlSignature += "<div style='border-width: medium medium 1pt; border-style: none none solid; border-color: rgb(204, 204, 204); padding: 0cm;'><p><span style='font-size: 9pt;'>&nbsp;</span></p></div><p><span style='font-size: 9pt;'></span></p></div>";

            string _htmlfooter = _mailFooter() +
                                          "</td>" +
                                        "</tr>" +
                                     "</table></td></tr></table>";

            if (Company.ToUpper() == "AGUAS DE MARRATXI")
                return _htmlheader + "Aguas de Marratxí le ha dado de alta en ZDocs  </br></br> Su nombre de usuario es su email, y su contraseña: " + Password + " , pero puede cambiarla cuando quiera entrando en perfil usuario " + _htmlSignature + _htmlfooter;
            else
                return _htmlheader + "Se le ha dado de alta en ZDocs  </br></br> Su nombre de usuario es su email, y su contraseña: " + Password + " , pero puede cambiarla cuando quiera entrando en perfil usuario" + _htmlSignature + _htmlfooter;
        }
        public static string _mailFooter()
        {
            string _footer = "<div style='font-size:10px; color: gray;'>";
            _footer += "<label><b>" + "Navegadores recomendados: Firefox, Chrome, Safari e Internet Explorer a partir de la versión 8." + "</b></label><br />";
            //_footer += "<label><b>" + _("Versión Beta Privada del servicio ZERTIFIKA.", c) + "</b></lable><br />";
            //_footer += "<label>" + _("Le informamos que temporalmente el ´servicio´ se proporciona en forma de versión beta privada ", c) + "</lable>";
            //_footer += "<label>" + _("y que se pone a disposición de los usuarios ´tal cual´ y en función de su disponibilidad, con la ", c) + "</lable>";
            //_footer += "<label>" + _("finalidad de proporcionar a ZERTIFIKA información acerca de la calidad y utilidad del mismo. El ", c) + "</lable>";
            //_footer += "<label>" + _("servicio puede contener errores o imprecisiones que pudieran provocar fallos, o incluso el borrado ", c) + "</lable>";
            //_footer += "<label>" + _("o pérdida de datos. Asimismo, el mantenimiento técnico y el soporte para el servicio, se realiza a ", c) + "</lable>";
            //_footer += "<label>" + _("través de medios online y en la página web.", c) + "</lable>";
            //_footer += "</div>";
            return _footer;
        }
        public static string EnviarCorreo(string Destino, string Asunto, string Texto, string Ruta)//,string Ruta2)
        {

            string Host = "mail.zertifika.com";
            string Origen = "info@zertifika.com";
            string Pass = "r3dM#z41#z41#z41";
            string User = "info@zertifika.com";
            if (Destino != "prevencioriscslaborals@palma.es" && Destino != "mcalvino@globalia.com" && Destino != "serprev11@globalia.com" && Destino != "previs.previs@gmail.com")
            {
                if (Destino.Contains("Zertifika.com"))
                    Destino = Destino;
                MailMessage vCorreo = new MailMessage(Origen, Destino, Asunto, Texto);
                vCorreo.IsBodyHtml = true;
                //if (System.IO.File.Exists(Ruta))// && System.IO.File.Exists(Ruta2))
                //{
                //    Attachment vAdjunto = null;
                //    if (Ruta != null)
                //    {
                //        vAdjunto = new Attachment(Ruta);
                //        vCorreo.Attachments.Add(vAdjunto);
                //    }                

                SmtpClient vCliente = new SmtpClient(Host);
                vCliente.Credentials = new System.Net.NetworkCredential(User, Pass);

                try
                {
                    vCliente.Send(vCorreo);
                    //txtenviados.Text += "\r\n" + Destino + DateTime.Now.ToString();
                    return "";

                }
                catch
                {
                    return "\r\n" + Destino + DateTime.Now.ToString();

                }
            }
            else
                return "";
         
        }
        public static string CompartirCon(string IdUsuarioComparte,string IdUsuarioNuevo,string IdEmpresa)
        {
            try
            {
                string Error="";
                string ObligadaLectura = "False";
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string SQL = "Select * from Expedientes where idusuario=" + IdUsuarioComparte ;//GG
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

               // DataTable datos = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, CadenaConexionAnywhere);
                
                if (ds.Tables[0].Rows.Count > 0)
                {
                     for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string IdExpediente= ds.Tables[0].Rows[i].ItemArray[0].ToString() ;
                        if (WS.ExisteExpedienteUsuario(IdExpediente, IdUsuarioNuevo)=="")
                           Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, IdUsuarioNuevo);                                            
                    
                        string SQL2 = "Select * from DocumentosExpedientes where idexpediente=" + IdExpediente ;
                        OleDbCommand vComando2 = new OleDbCommand(SQL2, vCon);

                        OleDbDataAdapter da2 = new OleDbDataAdapter();
                        DataSet ds2 = new DataSet();

                        da2.SelectCommand = vComando2;
                        da2.Fill(ds2);

                        //DataTable datos2 = DatabaseConnection.executeNonQueryDT(SQL2, CommandType.Text, CadenaConexionAnywhere);

                        for (int i2 = 0; i2 < ds2.Tables[0].Rows.Count; i2++)
                        {
                            string Iddocumento = ds2.Tables[0].Rows[i2].ItemArray[0].ToString();
                            if (! WS.ExisteDocumentoUsuario(Iddocumento,IdUsuarioNuevo))
                               Error = WS.InsertarDocumentoenDocumentosUsuario(Iddocumento, IdUsuarioNuevo , "Documentos Empresa", IdUsuarioComparte, ObligadaLectura, "", "", "", "", IdEmpresa);
                        }
                     }
                }
                else
                    return "-1";

               
                return "";
            }
            catch (OleDbException ex)
            {
                return "-1";
            }
        }
        public static void BorrarPdfdeDisco(string Ruta)
        {
            try {
                if (File.Exists(Ruta))
                    File.Delete(Ruta);                
            }
            catch { }
        }
        public static string  AsignarDocumentos()
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string Consulta = "select *  FROM Documentos WHERE (idUsuario = 104) AND (idDocumento NOT IN (SELECT idDoc FROM TiposDocumento WHERE (idDoc = Documentos.idDocumento)))";


                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                string Error="";
                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                    for (int i = 0; i <= ds.Tables[0].Rows.Count; i++)
                    {
                        Error = WS.ActualizarDocumentosTipos("2", ds.Tables[0].Rows[i].ItemArray[0].ToString(), "", "Documentos");
                        if (!string.IsNullOrEmpty(Error))
                            Error = Error;
                    }
                vCon.Close();
                return "";
            }
            catch { return "error"; }
        }
        public static void EliminarExpedienteUsuario(string IdExpediente,string IdUsuario)
        {
            try {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string SQL = "delete from ExpedientesUsuarios where idexpediente=" + IdExpediente + " and IdUsuario=" + IdUsuario;
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                int resultado = vComando.ExecuteNonQuery();                                          
            }
            
            catch { }
        }
        public static void EliminarDocumentoExpediente(string IdExpediente,string IdDocumento)
        {
            try {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string SQL = "delete from DocumentosExpedientes where idexpediente=" + IdExpediente + " and IdDocumento=" + IdDocumento;
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                int resultado = vComando.ExecuteNonQuery();                                          
            }
            
            catch { }
        }
        public static void EliminarDocumentoUsuario(string IdUsuario,string IdDocumento)
        {
            try {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string SQL = "delete from DocumentosUsuarios where idusuario=" + IdUsuario + " and IdDocumento=" + IdDocumento;
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                int resultado = vComando.ExecuteNonQuery();                                          
            }
            
            catch { }
        }
        public static void EliminarDocumento(string IdDocumento)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string SQL ="delete from documentosusuarios where iddocumento=" + IdDocumento;
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                int resultado = vComando.ExecuteNonQuery();
                SQL = "delete from documentos where iddocumento=" + IdDocumento;
                vComando = new OleDbCommand(SQL, vCon);
                resultado = vComando.ExecuteNonQuery();
            }

            catch { }
        }
        public static int ActualizarNombreDocumento(string NombreBueno,string IdUsuario, string IdDocumento)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string SQL = "update Documentos set DocOriginal='" + NombreBueno + "' where idusuario=" + IdUsuario + " and IdDocumento=" + IdDocumento;
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                int resultado = vComando.ExecuteNonQuery();
                return resultado;
            }

            catch { return -1; }
        }
        public static string DevolverExpedienteDocumento(string iddocumento)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                //Guid Idtoken = Guid.NewGuid();
                string Consulta = "select exp.descripcion from expedientes exp inner join documentosexpedientes docexp on docexp.idexpediente=exp.idexpediente where docexp.iddocumento=" + iddocumento;


                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count == 1)
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                

                 vCon.Close();
                return "";
            }
            catch { return "error"; }
        }
        public static bool ExisteMatricula(string Matricula)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);               
                string Consulta = "select Matricula,Texto,Centro,dni from matriculas where Matricula='" + Matricula + "'";               

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count == 0)
                    return false;
                else
                    return true;
                
                
            }
            catch { return false; }
        }
        public static bool ExisteDni(string Dni)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string Consulta = "select Matricula,Texto,Centro,dni from matriculas where Dni='" + Dni + "'";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count == 0)
                    return false;
                else
                    return true;


            }
            catch { return false; }
        }
        public static string DevolverMatriculaParamDesconocido(string Dato)
        { //NO SABEMOS SI RECIBIMOS EL DNI O LA MATRICULA
            string Dato1 = string.Empty;
            char Caracter='|';
            if (Dato.Contains('.'))
                Caracter = '.';
            else
                if (Dato.Contains('_'))
                    Caracter ='_';

           
            if (NumCaracteres(Dato,  Caracter) >= 1)
                Dato1 = Dato.Split(Caracter)[1];
            else
                Dato1 = Dato.Split(Caracter)[0];

            string Matricula="", Dni = "";
            //string Expediente = DevolverExpedienteDocumento(IdDocumento);

            //if (Expediente.Length > 10)
            //    Dni = Expediente.Split('_')[1];
            //else
            //    Dni = Expediente;
            //***************************OPCION 1 sacamos la matricula a partir del dni ****************************************                            
            // Matricula = Funciones.DevolverMatricula(Dni,"Matricula","");
            //opcion2 - COJO LA MATRICULA Y VOY A BUSCAR EL DNI
            //***************************fin OPCION 1 sacamos la matricula a partir del dni ****************************************


            //***************************OPCION 2 CONTEMPLAMOS MAS OPSIBILIDADES ****************************************
            //string Dato = NomActual.Split('-')[0];
            string Temp = string.Empty;
            if (Dato1.Length == 7) //MATRICULA NORMAL
                Matricula = Dato;
            else
            {
                if (Dato1.Length == 8 && Dato1.Substring(0, 1).ToUpper() == "X")  //MATRICULA CON UNA X DELANTE
                {
                    Matricula = Dato1.Substring(1, 7);
                }
                else
                {
                    if (Dato1.Length == 9)   //DNI CORRECTO
                    {
                        Temp = Funciones.DevolverMatricula(Dato1, "Matricula", "");
                        if (!string.IsNullOrEmpty(Temp))
                            if (ExisteMatricula(Temp))
                                Matricula = Temp;
                    }
                    else
                    {
                        if (Dato1.Length == 10)
                        {
                            Matricula = Funciones.DevolverMatricula(Dato1.Substring(1, 9), "Matricula", "");
                        }
                        else
                        {
                            if (Dato1.Length == 11)
                            {
                                Matricula = Funciones.DevolverMatricula(Dato1.Substring(2, 9), "Matricula", "");
                            }
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(Matricula))
                return Matricula;
            else
                if (!string.IsNullOrEmpty(Dni))
                    return  Dni;
            return "";
        }
        public static string DevolverMatricula(string Dni,string Campo,string Matricula)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                //Guid Idtoken = Guid.NewGuid();
                string Consulta = "";
                if (!string.IsNullOrEmpty(Dni))
                    Consulta="select Matricula,Texto,Centro,dni,Nombre from matriculas where Dni like '%" + Dni + "'";
                else
                    Consulta = "select Matricula,Texto,Centro,dni,Nombre from matriculas where Matricula='" + Matricula + "'";
                string Resul = "";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count == 1)
                {
                    switch (Campo)
                    {
                        case "MATRICULA":
                            Resul = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                            break;
                        case "TEXTO":
                            Resul = ds.Tables[0].Rows[0].ItemArray[1].ToString();
                            break;
                        case "DNI":
                            Resul = ds.Tables[0].Rows[0].ItemArray[3].ToString();
                            break;
                        case "CENTRO":
                            Resul = ds.Tables[0].Rows[0].ItemArray[2].ToString();
                            break;
                        case "NOMBRE":
                            Resul = ds.Tables[0].Rows[0].ItemArray[4].ToString();
                            break;
                    }
                }                   
                vCon.Close();
                return Resul;                
            }
            catch { return "error"; }
        }

        public static string DevolverIdDocumentodeNombreOriginal(string NombreOriginal)
        {
            try
            {                
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                //Guid Idtoken = Guid.NewGuid();
                string Consulta = "select IdDocumento from documentos where activo='True' and nombreoriginal='" + NombreOriginal + "'";


                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();                
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count == 1 )
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                
                
                vCon.Close();
                return "";
            }
            catch { return "error"; }
        }
        public static void RenombrarDocumentosDniEspecifico(string DniMalo,string DniBueno)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string Consulta = "select iddocumento,docoriginal from documentos where idusuario=104 and docoriginal like '%" + DniMalo + "%'";


                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string IdDocumento = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                        string NomActual = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                        string Tipo = "";
                        string DniOld = "";
                        string Resto="";
                        string[] Campos= null;
                         if (NomActual.Contains("-"))
                         {
                             Campos = BuscarCaracter(NomActual, "-").Split('|');
                             if (Campos.Length == 1)
                             {
                                 Tipo = NomActual.Split('-')[0];
                                 DniOld = NomActual.Split('-')[1];
                                 DniOld = DniOld.Split('.')[0];
                             }
                             else
                             {
                                if (Campos.Length > 1)
                                {
                                    Tipo = NomActual.Substring(0,Convert.ToInt16(Campos[Campos.Length-1]));
                                    DniOld = NomActual.Substring(Convert.ToInt16(Campos[Campos.Length - 1]) +1).Split('.')[0];
                                }
                             }
                         }
                         else
                         {
                             if (NomActual.Contains("_"))
                             {
                                 Campos = BuscarCaracter(NomActual, "_").Split('|');
                                 if (NomActual.Split('_')[0]==DniMalo)
                                 {
                                     DniOld = NomActual.Split('_')[0];
                                     Resto = NomActual.Substring(Convert.ToInt16(Campos[0]));
                                 }
                             }
                         }
                        
                        
                        if (!string.IsNullOrEmpty(DniOld) && DniOld == DniMalo)
                        {
                            if (NomActual.Contains("-"))
                               ActualizarNombreDocumento(Tipo + "-" + DniBueno + ".pdf", "104", IdDocumento);
                            else
                                if (NomActual.Contains("_"))
                                    ActualizarNombreDocumento(DniBueno + Resto, "104", IdDocumento);
                        }
                        else
                            DniBueno = DniBueno;
                    }

            }
            catch { }
        }
        public static string BuscarCaracter(string Cadena, string Caracter)
        {
            string Resul = "";
            try {
                for (int i = 0; i < Cadena.Length; i++)
                {
                    if (Cadena.Substring(i, 1) == Caracter)
                    {
                        if (!string.IsNullOrEmpty(Resul))
                            Resul+="|";
                        Resul += i.ToString();
                    }
                }
                return Resul;
            }
            catch { return ""; }
        }
       
        public static string TieneMetadatos(string IdTipo,string IdDocumento)
        {
            try
            {                
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string Res =string.Empty;
                string Consulta = "select Idmetadato from tiposdocumento where activo='True' and IdDoc=" + IdDocumento + " and IdtipoDocumento=" + IdTipo;


                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();                
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count == 1 )
                    Res= ds.Tables[0].Rows[0].ItemArray[0].ToString();
                else
                    Res= "";
                
                vCon.Close();
                return Res;
            }
            catch { return ""; }
        }
        public static string TieneValorCampo(string Tabla, string Campo,string CampoFiltro,string ValorCampoFiltro )
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string Res = string.Empty;
                string Consulta = "select " + Campo + " from " + Tabla + " where " + CampoFiltro + "='" + ValorCampoFiltro + "'";


                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count == 0)
                    Res ="";
                else
                    Res = ds.Tables[0].Rows[0].ItemArray[0].ToString();

                vCon.Close();
                return Res;
            }
            catch { return ""; }
        }
        public static string DevolverNombreDoc(string IdDocumento)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string Res = string.Empty;
                string Consulta = "select docOriginal from documentos where iddocumento=" + IdDocumento ;


                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count == 0)
                    Res = "";
                else
                    Res = ds.Tables[0].Rows[0].ItemArray[0].ToString();

                vCon.Close();
                return Res;
            }
            catch { return ""; }
        }
        public static string ActualizarMetadatosDocumento(string IdDoc,Boolean SoloCentro,Boolean ComprobrarPrimerDigito)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string Consulta2 = "";
                if (!string.IsNullOrEmpty(IdDoc))
                    Consulta2 = "select idDocumento, FechaCreacion,  docOriginal, NombreOriginal from documentos where idusuario=" + UsuarioCarrefour + " and iddocumento=" + IdDoc;
                else
                    Consulta2 = "select idDocumento, FechaCreacion,  docOriginal, NombreOriginal from documentos where idusuario=" + UsuarioCarrefour + " and left(docoriginal,1)='x'";// and Iddocumento in (select iddoc from tiposdocumento where idtipodocumento=7 )"; //and TieneIncidencia='True' 

                //Consulta2 = "Select doc.idDocumento, doc.FechaCreacion,  doc.docOriginal, doc.NombreOriginal from documentos doc inner join documentosexpedientes docexp on docexp.iddocumento=doc.iddocumento inner join expedientes exp on exp.idexpediente=docexp.idexpediente where (exp.descripcion like '%X9999635V' OR exp.descripcion like '%46035193A')";
               // Consulta2 = "select idDocumento, FechaCreacion,  docOriginal, NombreOriginal from documentos where idusuario=" + UsuarioCarrefour + " and (  (LEN(docoriginal)<15) and ( right(docoriginal,6) <>'CV.pdf' )  )   order by iddocumento desc";// len(docoriginal)<14 order by iddocumento desc ";// fechacreacion>'01/05/20106' and (idcentro is null or len(docoriginal)<15)";// and iddocumento IN (SELECT iddoc from tiposdocumento where iddoc=documentos.iddocumento and idtipodocumento>=1 and idtipodocumento<=100 and Activo='True')";
                //  Consulta2 = "select idDocumento, FechaCreacion,  docOriginal, NombreOriginal from documentos where activo='True' and idusuario=" + UsuarioCarrefour + " and (Iddocumento=25062 or Iddocumento=25444 or iddocumento=25636 or iddocumento=56684)";// and docoriginal like '%50994043F%'";
                //Consulta2 = "select idDocumento, FechaCreacion,  docOriginal, NombreOriginal from documentos where activo='True' and idusuario=" + UsuarioCarrefour + " and left(docoriginal,1)='x'"; //and TieneIncidencia='True'";// and docoriginal like '%50994043F%'";
                OleDbCommand vComando2 = new OleDbCommand(Consulta2, vCon);
                vCon.Open();
                OleDbDataAdapter da2 = new OleDbDataAdapter();
                DataSet ds2 = new DataSet();

                da2.SelectCommand = vComando2;
                da2.Fill(ds2);
                string MatriculaAnterior = "";

                for (int i2 = 0; i2 < ds2.Tables[0].Rows.Count; i2++)
                {                  
                    string Dni = "";
                    string Matricula = "";
                    string NomActual = ds2.Tables[0].Rows[i2].ItemArray[2].ToString();
                    string FechaCreacion = ds2.Tables[0].Rows[i2].ItemArray[1].ToString();
                    string Tipo = WS.DevolverIdTipoDocumento(UsuarioCarrefour, ds2.Tables[0].Rows[i2].ItemArray[0].ToString());
                    string IdTipo = "";
                    string IdMetadato = "";
                    string Centro = "";
                    string Digito = NomActual.Substring(1, 1);
                    //if  ( ComprobrarPrimerDigito && ( !( (Encoding.ASCII.GetBytes(NomActual.Substring(0, 1))[0] >= 48) && (Encoding.ASCII.GetBytes(NomActual.Substring(0, 1))[0] <= 57)) ))
                    if (ComprobrarPrimerDigito )
                    {

                        if (!string.IsNullOrEmpty(Tipo))
                        {
                            string[] Campos = BuscarCaracter(Tipo, "-").Split('|');
                            if (Campos.Length == 1)
                            {
                                IdTipo = Tipo.Split('|')[0];
                                IdMetadato = Tipo.Split('|')[1];
                            }

                            if (!string.IsNullOrEmpty(IdTipo))
                            {
                                string IdDocumento = "";
                                //if (string.IsNullOrEmpty(IdDoc))
                                IdDocumento = ds2.Tables[0].Rows[i2].ItemArray[0].ToString();
                                //else
                                //    IdDocumento = IdDoc;

                                string NomTabla = "c00000000";
                                NomTabla = NomTabla.Substring(0, 9 - IdTipo.Length) + IdTipo;
                                string Expediente = DevolverExpedienteDocumento(IdDocumento);

                                string MatriculaTemporal = "";
                                if (NomActual.Contains('-'))
                                    Matricula = NomActual.Split('-')[0];
                                if (Matricula.Contains(".pdf"))
                                    Matricula = Matricula.Replace(".pdf", "");

                                if (!ExisteMatricula(Matricula) && NumCaracteres(NomActual, '-') >= 1)
                                {
                                    MatriculaTemporal = NomActual.Split('-')[1];
                                    if (MatriculaTemporal.Contains(".pdf"))
                                        MatriculaTemporal = MatriculaTemporal.Replace(".pdf", "");
                                    if (ExisteMatricula(MatriculaTemporal))
                                        Matricula = MatriculaTemporal;
                                }
                                if (!ExisteMatricula(Matricula) && NumCaracteres(NomActual, '-') >= 1)
                                {
                                    MatriculaTemporal = NomActual.Split('-')[1];
                                    if (ExisteMatricula(MatriculaTemporal))
                                        Matricula = MatriculaTemporal;
                                }
                                //*********
                                if (!ExisteMatricula(Matricula) && NumCaracteres(NomActual, '_') >= 1)
                                {
                                    MatriculaTemporal = NomActual.Split('_')[1];
                                    if (ExisteMatricula(MatriculaTemporal))
                                        Matricula = MatriculaTemporal;
                                }
                                //*********
                                if (!ExisteMatricula(Matricula) && Matricula.Length == 9)
                                    Matricula = Funciones.DevolverMatricula(Matricula, "Matricula", "");

                                if (!ExisteMatricula(Matricula) && Matricula.Length == 8)
                                    Matricula = Matricula.Substring(1, 7);
                                if (!ExisteMatricula(Matricula))
                                    Matricula = Expediente.Split('_')[0];
                                if (!ExisteMatricula(Matricula) && Matricula.Length > 9 && Matricula.Substring(0, 1) == "0")
                                {
                                    Matricula = Matricula.Substring(1);
                                    Matricula = Funciones.DevolverMatricula(Matricula, "Matricula", "");
                                }
                                if (!ExisteMatricula(Matricula))
                                {
                                    Matricula = Funciones.DevolverMatricula(Matricula, "Matricula", "");

                                }
                                if (ExisteMatricula(Matricula))
                                {
                                    Dni = DevolverMatricula("", "Dni", Matricula);
                                    Centro = DevolverMatricula("", "Centro", Matricula);
                                    string DesTipo = WS.DevolverDescripcionTipo(IdTipo, "9", UsuarioCarrefour);

                                    if (NomActual != Matricula + "-" + DesTipo.Substring(6) + ".pdf")
                                        ActualizarNombreDocumento(Matricula + "-" + DesTipo.Substring(6) + ".pdf", "104", IdDocumento);

                                    if (IdTipo != "13" && IdTipo != "2" && IdTipo != "73" && ( ( Funciones.DevolverCampoTabla("IdCentro","Documentos"," Iddocumento=" + IdDocumento) != Funciones.DevolverCampoTabla("IdCentro","Centro", " Descripcion='" + Centro + "'") ) || (Dni != DevolverCampoTabla("Dni", NomTabla, " Idmetadato=" + IdMetadato)) || (Matricula != DevolverCampoTabla("Matricula", NomTabla, " Idmetadato=" + IdMetadato)) ))
                                    {
                                        if (string.IsNullOrEmpty(IdMetadato) || IdMetadato == "-1")
                                            IdMetadato = ActualizarMetadatosTablaX(NomTabla, Matricula, FechaCreacion, Dni, IdMetadato, Centro, SoloCentro);
                                        else
                                            ActualizarMetadatosTablaX(NomTabla, Matricula, FechaCreacion, Dni, IdMetadato, Centro, SoloCentro);

                                        ActualizarCampo("TiposDocumento", "idMetaDato", IdMetadato, " iddoc=" + IdDocumento + " and idtipodocumento=" + IdTipo + " and activo='True'");
                                        ActualizarCampo("Documentos", "CamposActualizados", "True", " iddocumento=" + IdDocumento);
                                        ActualizarCampo("Documentos", "TieneIncidencia", "False", " Iddocumento=" + IdDocumento);
                                        
                                    }
                                    if (!string.IsNullOrEmpty(Centro))
                                        ActualizarCampo("Documentos", "IdCentro", Funciones.DevolverCampoTabla("IdCentro", "Centro", " descripcion='" + Centro + "'"), " Iddocumento=" + IdDocumento);
                                }
                                else
                                { 
                                    ActualizarCampo("Documentos", "IdCentro", "0", " Iddocumento=" + IdDocumento);
                                    ActualizarCampo("Documentos", "Revisar", "True", " Iddocumento=" + IdDocumento);
                                    if (Matricula != MatriculaAnterior)
                                    {
                                        MatriculaAnterior = Matricula;
                                        //return "Matricula no Encontrada";
                                        
                                    }
                                   }
                                MatriculaAnterior = Matricula;
                                // }
                            }
                            else
                            {
                                Matricula = Matricula;
                                ActualizarCampo("Documentos", "Revisar", "'True'", " Iddocumento=" + IdDoc);
                                //  YA TIENE METADATO
                            }
                        }
                    }
                }



                return "";
            }

            catch (Exception e)
            { return "-1"; }
        }
        //************************************
        public static string ActualizarCentroenExpedientes()
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string SQL = "select idexpediente from expedientes where idusuario=104 and idcentro is null";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);


                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string SQL2 = " SELECT COUNT(*),doc.idCentro FROM documentos doc WHERE idDocumento IN (SELECT iddocumento FROM DocumentosExpedientes de WHERE de.idexpediente=" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + " ) GROUP BY doc.idCentro HAVING COUNT(*)>1";
                    OleDbCommand vComando2 = new OleDbCommand(SQL2, vCon);
                    OleDbDataAdapter da2 = new OleDbDataAdapter();
                    DataSet ds2 = new DataSet();

                    da2.SelectCommand = vComando2;
                    da2.Fill(ds2);

                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                        ActualizarCampo("Expedientes", "Idcentro", ds2.Tables[0].Rows[0].ItemArray[1].ToString(), " idexpediente=" + ds.Tables[0].Rows[i].ItemArray[0]);
                    }
                }
                return "1";
            }
            catch (Exception e)
            { return "0"; }
        }
        public static string ActualizarCentroenDocumentos( )
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string Consulta2 = "";
           
                Consulta2 = "select idDocumento, FechaCreacion,  docOriginal, NombreOriginal from documentos where activo='True' and (idcentro is null or idcentro='-1') and idusuario=" + UsuarioCarrefour ;
                Consulta2 = "select idDocumento, FechaCreacion,  docOriginal, NombreOriginal from documentos where activo='True' and (iddocumento=82805 or iddocumento= 91222) and idusuario=" + UsuarioCarrefour;
                Consulta2 = "select * from expedientes where idusuario=104";
                OleDbCommand vComando2 = new OleDbCommand(Consulta2, vCon);
                vCon.Open();
                OleDbDataAdapter da2 = new OleDbDataAdapter();
                DataSet ds2 = new DataSet();

                da2.SelectCommand = vComando2;
                da2.Fill(ds2);


                for (int i2 = 0; i2 < ds2.Tables[0].Rows.Count; i2++)
                {
                    string Dni = "";
                    string Matricula = "";
                    string NomActual = ds2.Tables[0].Rows[i2].ItemArray[2].ToString();
                    string FechaCreacion = ds2.Tables[0].Rows[i2].ItemArray[1].ToString();
                    string Tipo = WS.DevolverIdTipoDocumento(UsuarioCarrefour, ds2.Tables[0].Rows[i2].ItemArray[0].ToString());
                    string IdTipo = "";
                    string IdMetadato = "";
                    string Centro = "";
                    string Digito = NomActual.Substring(1, 1);
                    //if  ( ComprobrarPrimerDigito && ( !( (Encoding.ASCII.GetBytes(NomActual.Substring(0, 1))[0] >= 48) && (Encoding.ASCII.GetBytes(NomActual.Substring(0, 1))[0] <= 57)) ))                    

                    if (!string.IsNullOrEmpty(Tipo))
                    {
                        string[] Campos = BuscarCaracter(Tipo, "-").Split('|');
                        if (Campos.Length == 1)
                        {
                            IdTipo = Tipo.Split('|')[0];
                            IdMetadato = Tipo.Split('|')[1];
                        }
                        string IdDocumento = "";
                        IdDocumento = ds2.Tables[0].Rows[i2].ItemArray[0].ToString();
                        if (!string.IsNullOrEmpty(IdTipo) && IdTipo != "0")// && IdMetadato != "-1")
                        {

                            string NomTabla = "c00000000";
                            NomTabla = NomTabla.Substring(0, 9 - IdTipo.Length) + IdTipo;
                            //string Expediente = DevolverExpedienteDocumento(IdDocumento);
                            Centro = Funciones.DevolverCampoTabla(NomTabla, "Centro", " Idmetadato=" + IdMetadato);
                            if (!string.IsNullOrEmpty(Centro) && Centro != "-1")
                                ActualizarCampo("Documentos", "IdCentro", Funciones.DevolverCampoTabla("Centro", "Idcentro", " descripcion='" + Centro + "'"), " Iddocumento=" + IdDocumento);
                            else
                            { 
                               ActualizarMetadatosDocumento(IdDocumento, false, true);
                                Tipo = WS.DevolverIdTipoDocumento(UsuarioCarrefour, ds2.Tables[0].Rows[i2].ItemArray[0].ToString());
                                IdTipo = Tipo.Split('|')[0];
                                IdMetadato = Tipo.Split('|')[1];
                                NomTabla = "c00000000";
                                NomTabla = NomTabla.Substring(0, 9 - IdTipo.Length) + IdTipo;
                                Centro = Funciones.DevolverCampoTabla(NomTabla, "Centro", " Idmetadato=" + IdMetadato);
                                if (!string.IsNullOrEmpty(Centro))
                                    ActualizarCampo("Documentos", "IdCentro", Funciones.DevolverCampoTabla("Centro", "Idcentro", " descripcion='" + Centro + "'"), " Iddocumento=" + IdDocumento);

                            }                                  
                        }
                        else
                        {
                               
                            ActualizarCampo("Documentos", "Revisar", "'True'", " Iddocumento=" + IdDocumento);
                            //  YA TIENE METADATO
                        }
                    }
                    
                }



                return "";
            }

            catch (Exception e)
            { return "-1"; }
        }
        //**********************************
        public  static string ActualizarMetadatosPorExpediente()
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);                
                string Consulta = "select * from expedientes where idusuario=104 and activo='True' "; //fecha>'15/02/2016' and

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);
                int cont = 0;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string Consulta2 = "select idDocumento,  FechaCreacion,  docOriginal, NombreOriginal from documentos where activo='True' and fechacreacion>'01/03/2016' and iddocumento in ( select docexp.iddocumento from documentosexpedientes docexp where docexp.iddocumento=documentos.iddocumento and docexp.idexpediente=" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + ") ";

                        OleDbCommand vComando2 = new OleDbCommand(Consulta2, vCon);                        
                        OleDbDataAdapter da2 = new OleDbDataAdapter();
                        DataSet ds2= new DataSet();

                        da2.SelectCommand = vComando2;
                        da2.Fill(ds2);

                        if (ds2.Tables[0].Rows.Count > 0)
                        {
                            for (int i2 = 0; i2 < ds2.Tables[0].Rows.Count; i2++)
                            {
                                string Dni = "";
                                string Matricula = "";
                                string IdDocumento = ds2.Tables[0].Rows[i2].ItemArray[0].ToString();
                                string NomActual = ds2.Tables[0].Rows[i2].ItemArray[2].ToString();
                                string FechaCreacion = ds2.Tables[0].Rows[i2].ItemArray[1].ToString();
                                string Tipo = WS.DevolverIdTipoDocumento("104", IdDocumento);
                                string IdTipo = "";
                                string IdMetadato = "";
                                string Centro = "";
                                if (!string.IsNullOrEmpty(Tipo))
                                {
                                    string[] Campos = BuscarCaracter(Tipo, "-").Split('|');                                                                      
                                    if (Campos.Length == 1)
                                    {
                                        IdTipo = Tipo.Split('|')[0];
                                        IdMetadato = Tipo.Split('|')[1];
                                    }

                                    if (!string.IsNullOrEmpty(IdTipo))
                                    {
                                        if (IdMetadato == "-1")
                                        {
                                            string NomTabla = "c00000000";
                                            NomTabla = NomTabla.Substring(0, 9 - IdTipo.Length) + IdTipo;
                                            string Expediente = DevolverExpedienteDocumento(IdDocumento);
                                            string MatriculaTemporal = "";
                                            if (NomActual.Substring(0, 1).ToUpper() == "X")
                                                NomActual = NomActual.Substring(1);
                                            if (NomActual.Contains('-'))
                                                Matricula = NomActual.Split('-')[0];
                                            if (!ExisteMatricula(Matricula) && NumCaracteres(NomActual, '-') > 1)
                                            {
                                                MatriculaTemporal = NomActual.Split('-')[1];
                                                if (ExisteMatricula(MatriculaTemporal))
                                                    Matricula = MatriculaTemporal;
                                            }
                                            if (!ExisteMatricula(Matricula) && NumCaracteres(NomActual, '-') > 1)
                                            {
                                                MatriculaTemporal = NomActual.Split('-')[1];                                                
                                                if (ExisteMatricula(MatriculaTemporal))
                                                    Matricula = MatriculaTemporal;
                                            }
                                            //*********
                                            if (!ExisteMatricula(Matricula) && NumCaracteres(NomActual, '_') > 1)
                                            {
                                                MatriculaTemporal = NomActual.Split('_')[1];
                                                if (ExisteMatricula(MatriculaTemporal))
                                                    Matricula = MatriculaTemporal;
                                            }
                                            //*********
                                            if (!ExisteMatricula(Matricula) && Matricula.Length == 9)                                                                                            
                                                Matricula=Funciones.DevolverMatricula(Matricula, "Matricula", "");
                                            
                                            if (!ExisteMatricula(Matricula) && Matricula.Length == 8 )                                                
                                                 Matricula = Matricula.Substring(1,7);
                                            
                                            if (ExisteMatricula(Matricula))
                                            {
                                                Dni = DevolverMatricula("", "Dni", Matricula);
                                                Centro = DevolverMatricula("", "Centro", Matricula);
                                                //string Temp=DevolverMatriculaParamDesconocido(Expediente);
                                                //if (Expediente.Length > 10)
                                                //    Dni = Expediente.Split('_')[1];
                                                //else
                                                //    Dni = Expediente;
                                                //Matricula = Funciones.DevolverMatricula(Dni, "Matricula", "");


                                                if (IdTipo != "13" && IdTipo != "2" && IdTipo != "73")
                                                {
                                                    if (string.IsNullOrEmpty(IdMetadato) || IdMetadato == "-1")
                                                        IdMetadato = ActualizarMetadatosTablaX(NomTabla, Matricula, FechaCreacion, Dni, IdMetadato,Centro,false);
                                                    else
                                                        ActualizarMetadatosTablaX(NomTabla, Matricula, FechaCreacion, Dni, IdMetadato,Centro,false);

                                                    ActualizarCampo("TiposDocumento", "idMetaDato", IdMetadato, " iddoc=" + IdDocumento + " and idtipodocumento=" + IdTipo + " and activo='True'");
                                                    ActualizarCampo("Documentos", "CamposActualizados", "True", " iddocumento=" + IdDocumento);
                                                }
                                            }
                                            else
                                                ActualizarCampo("Documentos", "Revisar", "True", " Iddocumento=" + IdDocumento);

                                        }
                                    }
                                    else
                                    {
                                        Matricula = Matricula;
                                        ActualizarCampo("Documentos", "Revisar", "'True'", " Iddocumento=" + IdDocumento);
                                        //  YA TIENE METADATO
                                    }
                                }
                            }
                        }
                    }
                }
                return "";
            }

            catch { return "-1"; }
        }

        
        public static int NumCaracteres(string Cadena, char Caracter)
        {
            try
            {
                int cont = 0;
                for (int i = 0; i < Cadena.Length; i++)
                {
                    if (Convert.ToChar(Cadena.Substring(i, 1)) == Caracter)
                        cont++;
                }
                return cont;
            }
            catch
            { return -1; }
        }
        public static string DevolverUsuario(string Cliente)
        {
            string res = "";
            switch (Cliente)
            {
                case "W2M":
                    res = "409";
                    break;
                case "Carrefour":
                    res = "104";
                    break;
            }
            return res;
        }

        public static void RenombrarDocumentosTextoporTexto(string TextoActual, string textoNuevo,string IdUsuario)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);                
                string Consulta = "SELECT iddocumento,docoriginal  FROM Documentos WHERE  (idUsuario = " + IdUsuario + ") AND docoriginal like '%" + TextoActual + "%'";// (FechaCreacion > '15/02/2016') AND (FechaCreacion < '20/02/2016')" ;
                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);
                string FicheroErrores = "F:\\Web\\CARREFOUR\\Carrefour\\LOGS\\" + "ErroresRenombrado" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";
                if (ds.Tables[0].Rows.Count > 0)
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string Dni = "";
                        string Temp = "";
                        string Matricula = "";
                        string IdDocumento = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                        string NomActual = ds.Tables[0].Rows[i].ItemArray[1].ToString();                        

                        int Pos1 = NomActual.IndexOf(TextoActual);
                        string Resul = NomActual.Substring(0, Pos1) + textoNuevo + NomActual.Substring(Pos1+ TextoActual.Length) ;
                        if (!string.IsNullOrEmpty(Resul))
                            ActualizarNombreDocumento(Resul, UsuarioCarrefour, IdDocumento);
                       

                    }

            }
            catch { Exception e; }
        }
        public static void RenombrarDocumentos(string TextoActual,string IdDoc,string IdUsuario)
        {
            try { 
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                //string Consulta = "select iddocumento,docoriginal from documentos where activo='True' and idusuario=104  and  FechaCreacion > '15/02/2016'";// and docoriginal like '%afilici%'";//iddocumento in (select iddocumento from documentosexpedientes where idexpediente=23)";
                //string Consulta = "SELECT iddocumento,docoriginal  FROM Documentos WHERE (docOriginal LIKE '%20160216%' OR docOriginal LIKE '%20160217%') AND (idDocumento NOT IN (SELECT Documentos.idDocumento FROM TiposDocumento AS td INNER JOIN TipoDocumento AS tip ON tip.idTipoDocumento = td.idTipoDocumento AND tip.Padre = '-1' AND tip.activo = 'True' AND td.Activo = 'True' AND td.idDoc = Documentos.idDocumento)) AND (activo = 'true')";
                string Consulta = "";
                if (string.IsNullOrEmpty(IdDoc))
                     Consulta="SELECT iddocumento,docoriginal  FROM Documentos WHERE  (idUsuario = 104) AND docoriginal like '%" + TextoActual + "%'";// (FechaCreacion > '15/02/2016') AND (FechaCreacion < '20/02/2016')" ;
                else
                    Consulta = "SELECT iddocumento,docoriginal  FROM Documentos WHERE  (idUsuario = 104) AND Iddocumento =" + IdDoc ;
                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);
                string FicheroErrores = "C:\\Web\\CARREFOUR\\Carrefour\\LOGS\\" + "ErroresRenombrado" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";
                if (ds.Tables[0].Rows.Count >0)
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string Dni = "";
                        string Temp = "";
                        string Matricula = "";
                        string IdDocumento = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                        string NomActual= ds.Tables[0].Rows[i].ItemArray[1].ToString();
                        string IdTipo=WS.DevolverIdTipoDocumento("104", IdDocumento).Split('|')[0];
                        string DesTipo = WS.DevolverDescripcionTipo(IdTipo, "9", "104");
                        if (IdTipo == "2" || IdTipo == "13" || IdTipo == "21" || IdTipo == "26" || IdTipo == "32" || IdTipo == "45" || IdTipo == "56" || IdTipo == "73")
                            IdTipo = IdTipo;
                        if (!string.IsNullOrEmpty(DesTipo))
                        {
                            if (NumCaracteres(DesTipo,'.')>=2 )
                               DesTipo = DesTipo.Split('.')[2];
                            else
                                DesTipo = DesTipo.Split('.')[1];
                            string Expediente = DevolverExpedienteDocumento(IdDocumento);

                            if (Expediente.Length > 10)                           
                                Dni = Expediente.Split('_')[1];                                                          
                            else                           
                                Dni = Expediente;
                            //***************************OPCION 1 sacamos la matricula a partir del dni ****************************************                            
                            // Matricula = Funciones.DevolverMatricula(Dni,"Matricula","");
                            //opcion2 - COJO LA MATRICULA Y VOY A BUSCAR EL DNI
                            //***************************fin OPCION 1 sacamos la matricula a partir del dni ****************************************
                            string NomTabla = "c00000000";
                            NomTabla = NomTabla.Substring(0, 9 - IdTipo.Length) + IdTipo;
                            string FechaDocumento = Funciones.DevolverValorMetadato("FechaDocumento", NomTabla, IdDocumento, IdTipo, "");
                            if (string.IsNullOrEmpty(FechaDocumento))
                                FechaDocumento = Funciones.DevolverValorMetadato("[Fecha de Documento]", NomTabla, IdDocumento, IdTipo, "yyyymmdd");
                            //***************************OPCION 2 CONTEMPLAMOS MAS OPSIBILIDADES ****************************************
                            string Dato = "";
                            if (NomActual.Contains('-'))
                                Dato = NomActual.Split('-')[0];
                            else
                             if (NomActual.Contains('_'))
                                Dato = NomActual.Split('_')[1];
                            if (Dato.Length == 7) //MATRICULA NORMAL
                                Matricula = Dato;
                            else
                            {
                                if (Dato.Length == 8 && Dato.Substring(0, 1).ToUpper() == "X")  //MATRICULA CON UNA X DELANTE
                                {
                                    Matricula = Dato.Substring(1, 7);
                                }
                                else
                                {
                                    if (Dato.Length == 9)   //DNI CORRECTO
                                    {
                                        Temp = Funciones.DevolverMatricula(Dato, "Matricula", "");
                                        if (!string.IsNullOrEmpty(Temp))
                                            if (ExisteMatricula(Temp))
                                                Matricula = Temp;
                                    }
                                    else
                                    {
                                        if (Dato.Length == 10)
                                        {
                                            Matricula = Funciones.DevolverMatricula(Dato.Substring(1, 9), "Matricula", "");
                                        }
                                        else
                                        {
                                            if (Dato.Length == 11)
                                            {
                                                Matricula = Funciones.DevolverMatricula(Dato.Substring(2, 9), "Matricula", "");
                                            }
                                        }
                                    }
                                }
                            }
                            if (!ExisteMatricula(Matricula) || !string.IsNullOrEmpty(Expediente) || Expediente.Contains('_'))
                                Matricula = Expediente.Split('_')[0];  
                            if (string.IsNullOrEmpty(Matricula) || !ExisteMatricula(Matricula))                                
                            {
                                Dato = NomActual.Split('-')[1];
                                Dato = Dato.Replace(".pdf", "");
                                if (Dato.Length == 7) //MATRICULA NORMAL
                                    Matricula = Dato;
                                else
                                {
                                    if (Dato.Length == 8 && Dato.Substring(0, 1).ToUpper() == "X")  //MATRICULA CON UNA X DELANTE
                                    {
                                        Matricula = Dato.Substring(1, 7);
                                    }
                                    else
                                    {
                                        if (Dato.Length == 9)   //DNI CORRECTO
                                        {
                                            Temp = Funciones.DevolverMatricula(Dato, "Matricula", "");
                                            if (!string.IsNullOrEmpty(Temp))
                                                if (ExisteMatricula(Temp))
                                                    Matricula = Temp;
                                        }
                                        else
                                        {
                                            if (Dato.Length == 10)
                                            {
                                                Matricula = Funciones.DevolverMatricula(Dato.Substring(1, 9), "Matricula", "");
                                            }
                                            else
                                            {
                                                if (Dato.Length == 11)
                                                {
                                                    Matricula = Funciones.DevolverMatricula(Dato.Substring(2, 9), "Matricula", "");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //***************************FIN OPCION 2 CONTEMPLAMOS MAS OPSIBILIDADES ****************************************

                            //*************************** OPCION 3 NOS BASAMOS EN LA MATRICULA****************************************
                            //Matricula = NomActual.Split('-')[1];
                            //Dni = Funciones.DevolverMatricula("", "Dni", Matricula);
                            //*************************** FIN OPCION 3 NOS BASAMOS EN LA MATRICULA****************************************
                            string NombreNuevo = "";
                            if   (!string.IsNullOrEmpty(FechaDocumento))                            
                                NombreNuevo = FechaDocumento + "_" + Matricula + "_" + Dni + "_" + DesTipo;
                            else
                                NombreNuevo = "00000000" + "_" + Matricula + "_" + Dni + "_" + DesTipo;

                            if (ExisteMatricula(Matricula))
                            {
                                if (NomActual.ToUpper() !=  NombreNuevo + ".PDF")
                                //    if (NomActual.ToUpper() != Matricula.ToUpper() + "-" + DesTipo.ToUpper() + ".PDF")
                                {
                                    //if (string.IsNullOrEmpty(Matricula))
                                    //    ActualizarNombreDocumento(FechaDocumento + "_" + Dni + "_" + DesTipo + ".pdf", "104", IdDocumento);
                                    //else
                                    //    ActualizarNombreDocumento(FechaDocumento + "_" + Matricula + "_" + DesTipo + ".pdf", "104", IdDocumento);
                                    ActualizarNombreDocumento(NombreNuevo, IdUsuario, IdDocumento);
                                   ActualizarCampo("Documentos", "CamposActualizados", "True", " iddocumento=" + IdDocumento);
                                }
                            }
                            else
                            {
                                File.AppendAllText(FicheroErrores, "Error No se encuentra la mtricula: " + Matricula  + " en la BBDD" + Environment.NewLine );
                            }
                        }
                    }

            }
            catch (Exception e) { }
        }
        public static string EliminarExpediente(string NombreExpediente,string IdUsuario)
        {
            try
            {
                string IdExpediente = WS.ExisteExpedienteUsuarioPorNombre(IdUsuario, NombreExpediente);
                if (!string.IsNullOrEmpty(IdExpediente))
                {
                    Funciones.ActualizarCampo("Expedientes", "Activo", "False", " idexpediente=" + IdExpediente );
                    OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                    string Consulta = "select iddocumento from documentosexpedientes where idexpediente=" + IdExpediente;


                    OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                    vCon.Open();

                    OleDbDataAdapter da = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    da.SelectCommand = vComando;
                    da.Fill(ds);

                    if (ds.Tables[0].Rows.Count >0)
                        for (int i = 0; i <= ds.Tables[0].Rows.Count; i++)
                        {
                            Funciones.ActualizarCampo("Documentos", "Activo", "False", " iddocumento=" + ds.Tables[0].Rows[i].ItemArray[0].ToString());                            
                        }
                    else
                        return "No hay ningun documento en este expediente";

                    return "";
                }
                else
                {
                    return "No Existe el Expediente";
                }

            }
            catch { return "error"; } 
        }

        public static string EliminarDocumentos(Double IdInicial,Double IdFinal)
        {
            try
            {
                if (IdInicial>0 && IdFinal>0)                
                {
                   
                    OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                    string Consulta = "select iddocumento from documentos where iddocumento>=" + IdInicial + " and Iddocumento<=" + IdFinal ;


                    OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                    vCon.Open();

                    OleDbDataAdapter da = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    da.SelectCommand = vComando;
                    da.Fill(ds);

                    if (ds.Tables[0].Rows.Count > 0)
                        for (int i = 0; i <= ds.Tables[0].Rows.Count-1; i++)
                        {
                            string Iddocumento = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                            EliminarDocumento(Iddocumento);
                            string Ruta = "00000000";
                            Ruta = Ruta.Substring(0, 8 - Iddocumento.Length) + Iddocumento;
                           
                            string pathString = Ruta.Substring(0, 2) + "\\" + Ruta.Substring(2, 2) + "\\" + Ruta.Substring(4, 2) + "\\" + Ruta.Substring(6, 2) + ".pdf";// +Ruta.Substring(6, 2) + "/";
                            string Servidor = @"\\192.168.1.197\c$\inetpub\wwwroot\signesProVolumenes\";
                            pathString = Servidor + pathString;
                            System.IO.File.Delete(pathString);
                           
                        }
                    else
                        return "No hay ningun documento en este rango";

                    return "";
                }
                else
                {
                    return "Ha de Introducir un IdInicial y un IdFinal";
                }

            }
            catch (Exception e) { return "error"; }
        }
        public string DevolverRutaDocumento(string Iddocumento)
        {
            string Ruta = "";
            string Path = "/Inetpub/wwwroot/signesProVolumenes/";
            Ruta = "00000000";
            Ruta = Ruta.Substring(0, 8 - Iddocumento.Length) + Iddocumento;
            //string pathString = Funciones.Ruta + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/" + idDoc + Doc.Formato;// +Ruta.Substring(6, 2) + "/";
            string pathString = Path + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/" + Ruta.Substring(6, 2) + ".pdf";
            return pathString;

        }
        
        public static string DevolverCampoTabla( string Campo, string Tabla,string Criterio)
        {
            try
            {

                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string Consulta = "select " + Campo + " from " + Tabla + " where " + Criterio;

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    vCon.Close();
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                }
                vCon.Close();
                return "-1";
            }
            catch (Exception e)
            { return "-1"; }
        }
        public static string ActualizarCampo(string Tabla, string NomCampo, string ValorCampo, string Criterio)
        {
            try
            {
                int resultado;
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string Consulta = "update " + Tabla + " set " + NomCampo + "='" + ValorCampo + "' where " + Criterio;

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteNonQuery();

                vCon.Close();
                return "1";
            }
            catch (Exception e)
            { return "-1"; }
        }

        public static string ActualizarMetadatosTablaX(string Tabla, string Matricula, string Fecha, string Dni,string IdMetadato,string Centro,Boolean SoloCentro)
        {
            try
            {
                string resultado = "";
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string Consulta = "";
                if (IdMetadato != "-1" && !string.IsNullOrEmpty(IdMetadato))   //recien puesto  ! string.Is  , antes ||
                {
                    if (SoloCentro)
                    {
                        if (TieneValorCampo(Tabla, "Centro", "IdMetadato", IdMetadato) == "")
                            Consulta = "update " + Tabla + " set Centro='" + Centro + "' where idMetadato=" + IdMetadato;
                        else
                            Centro = Centro;
                    }
                    else
                        Consulta = "update " + Tabla + " set Matricula='" + Matricula + "', Dni='" + Dni + "', [Fecha de Recepción]='" + Fecha + "',Centro='" + Centro + "' where idMetadato=" + IdMetadato;
                }
                else
                    Consulta = "insert into " + Tabla + " (Matricula,Dni,[Fecha de Recepción],Centro) values ('" + Matricula + "','" + Dni + "', '" + Fecha + "','" + Centro + "') SELECT @@IDENTITY ";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                if (IdMetadato != "-1" && !string.IsNullOrEmpty(IdMetadato))
                   vComando.ExecuteNonQuery();// .ExecuteNonQuery();
                else
                    resultado=vComando.ExecuteScalar().ToString();// .ExecuteNonQuery();

                vCon.Close();
                return resultado;
            }
            catch (Exception e)
            { return "-1"; }
        }
        public static string DevolverIdTipo(string Tipo, string IdUsuario, string Tabla, string IdEmpresa)
        {  //ACTUALIZADO
            try
            {
                if (!string.IsNullOrEmpty(Tipo))
                {

                    OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                    string SQL = "";
                    if (Tabla == "Expedientes")
                        SQL = "select idtipoexpediente from Tipoexpediente where descripcion like '%" + Tipo + "%' and (Idusuario='" + IdUsuario + "' or idempresa=" + IdEmpresa + ")";
                    else
                        SQL = "select idtipodocumento from Tipodocumento where descripcion like '%" + Tipo + "%' and (Idusuario='" + IdUsuario + "' or idempresa=" + IdEmpresa + ")";
                    OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                    vCon.Open();
                    OleDbDataAdapter da = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    da.SelectCommand = vComando;
                    da.Fill(ds);

                    //DataTable datos = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionAnywhereLocal);
                    if (ds.Tables[0].Rows.Count > 0)
                        return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                    else
                        return "";

                    //if (datos.Rows.Count > 0)
                    //    if (Tabla == "Expedientes")
                    //        return datos.Rows[0]["idtipoExpediente"].ToString();
                    //    else
                    //        return datos.Rows[0]["idtipoDocumento"].ToString();
                    //else
                    //    return "";

                }
                else
                    return "";
            }
            catch (OleDbException ex)
            {
                return "";
            }
        }
        public static void ActualizarPorIds(string Fich)
        {
            string[] Fichero = File.ReadAllLines(Fich);
            foreach (string Linea in Fichero)
            {
                string[] Campos = Linea.Split('|');
                if (!string.IsNullOrEmpty(Campos[5]))
                {
                    Campos[5] = Campos[5].Replace("\"", "");
                    Campos[5] = Campos[5].Replace(".tif", "");
                    Campos[5] = Campos[5].Substring(0, Campos[5].IndexOf("-"));
                    string Datos = WS.DevolverIdTipoDocumento(UsuarioCarrefour, Campos[5]);
                    //SACAMOS EL TIPO QUE TIENE ACTUALMENTE
                    string TipoActual = Datos.Split('|')[0];
                    string MetadatoActual = Datos.Split('|')[1];
                    //Y CUAL ES EL NUEVO TIPO QUE HAY QUE ASIGNARLE
                    Campos[3]=Campos[3].Replace("#", "/");
                    string NuevoTipo=DevolverIdTipo(Campos[3].Replace("\"",""), UsuarioCarrefour, "Documentos","9");
                    //Y AHORA LOS METADATOS QUE TIENE ACTUALMENTE
                    //    string Metadatos=WS.DevolverCampos(TipoActual, "",Campos[5],MetadatoActual);
                    // Dictionary<string, string> _Campos = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(Metadatos);
                    //CAMBIAMOS EL TIPO DEL DOCUMENTO
                    if (string.IsNullOrEmpty(NuevoTipo))
                        NuevoTipo = NuevoTipo;
                    WS.ActualizarDocumentosTipos(NuevoTipo, Campos[5],"","Documentos");

               //     string MetadatosNuevos = WS.DevolverCampos(NuevoTipo, "", Campos[5], MetadatoActual); 
                    string NomTabla = "c00000000";
                    NomTabla = NomTabla.Substring(0, 9 - NuevoTipo.Length) + NuevoTipo;
                    Funciones.ActualizarMetadatosDocumento(Campos[5], false,true);
                   // RenombrarDocumentos(DevolverNombreDoc(Campos[5]));
                        
                    Funciones.ActualizarCampo("Documentos", "Revisar", "True", " Iddocumento=" + Campos[5]);
                    //foreach (KeyValuePair<string, string> field in _Campos)
                    //{
                    //    string Centro = ""; string Matricula = ""; string FechaCreacion =  "";string Dni = "";string FechaInicio = "";string FechaFin = "";
                    //    switch ((field.Key).ToUpper())
                    //    {
                    //        case "CENTRO":
                    //            Centro = field.Value;
                    //            break;
                    //        case "MATRICULA":
                    //            Matricula = field.Value;
                    //            break;
                    //        case "DNI":
                    //            Dni = field.Value;
                    //            break;
                    //        case "FECHACREACION" :
                    //            FechaCreacion = field.Value;
                    //            break;
                    //        case "FECHA INICIO":
                    //            FechaInicio = field.Value;
                    //            break;
                    //        case "FECHA FIN":
                    //            FechaFin = field.Value;
                    //            break;
                    //    }
                    //   // WS.ActualizarCampo(NomTabla, field.Key, field.Value, "IdMetadato=");

                    //    string IdMetadato = ActualizarMetadatosTablaX(NomTabla, Matricula, FechaCreacion, Dni, "", Centro, false);
                    //    if (!string.IsNullOrEmpty(FechaInicio))
                    //        WS.ActualizarCampo(NomTabla, "Fecha Inicio", FechaInicio, " Idmetadato=" + IdMetadato);
                    //    if (!string.IsNullOrEmpty(FechaFin))
                    //        WS.ActualizarCampo(NomTabla, "Fecha Fin", FechaFin, " Idmetadato=" + IdMetadato);
                    //}

                }

            }
        }
        public static void RenombrarExpedientes(string TextoActual,string TextoCambiar,string IdUsuario)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexionAnywhere);
                string Consulta = "";
                if (!string.IsNullOrEmpty(TextoActual))
                    Consulta="select idexpediente,descripcion from expedientes where descripcion='" + TextoActual + "' and activo='True' and idusuario=104" ;
                else
                    Consulta= "select idexpediente,descripcion from expedientes where  activo='True' and idusuario=" + IdUsuario ;

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);
                int Num_Docs = 0;

                if (ds.Tables[0].Rows.Count > 0)
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string Dni = "";
                        string NombreCompleto = "";
                        string IdExpediente = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                        if (IdExpediente == "517")
                            IdExpediente = IdExpediente;
                        string NomActual = ds.Tables[0].Rows[i].ItemArray[1].ToString();                        
                        if (!string.IsNullOrEmpty(IdExpediente))
                        {

                            if (NomActual.Contains('_'))
                            { 
                                Dni = NomActual.Split('_')[1];
                                if (Dni.Substring(0, 1) == "0" && Dni.Length>9)
                                    Dni = Dni.Substring(1);
                                NombreCompleto = Funciones.DevolverMatricula(Dni, "MATRICULA", "") + "_" + Funciones.DevolverMatricula(Dni, "DNI", "") + "_" + Funciones.DevolverMatricula(Dni, "NOMBRE", "");
                            }
                            else
                                NombreCompleto = Funciones.DevolverMatricula(Dni, "MATRICULA", "") + "_" + Funciones.DevolverMatricula(Dni, "DNI", "") + "_" + Funciones.DevolverMatricula(Dni, "NOMBRE", "");
                            //NombreCompleto = Funciones.DevolverMatricula(NomActual, "Texto", "");


                            if (!string.IsNullOrEmpty(NombreCompleto))
                            {
                                if (NomActual.ToUpper() != NombreCompleto && !string.IsNullOrEmpty(NombreCompleto))
                                    ActualizarCampo("Expedientes", "Descripcion", NombreCompleto, " idexpediente=" + IdExpediente);
                                //string SQL = "select * from documentosexpedientes docexp inner join documentos doc on doc.iddocumento=docexp.iddocumento where doc.docoriginal like 'X%' and docexp.idexpediente=" + ds.Tables[0].Rows[i].ItemArray[0].ToString();
                                string SQL = "select * from documentosexpedientes docexp inner join documentos doc on doc.iddocumento=docexp.iddocumento where docexp.idexpediente=" + ds.Tables[0].Rows[i].ItemArray[0].ToString();
                                DataTable datos = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, CadenaConexionAnywhereSinProvider);
                                string IdDocumento = "";
                                if (datos.Rows.Count > 0)
                                {
                                    for (int cont = 0; cont < datos.Rows.Count; cont++)
                                    {
                                        //if  ( ComprobrarPrimerDigito && ( !( (Encoding.ASCII.GetBytes(NomActual.Substring(0, 1))[0] >= 48) && (Encoding.ASCII.GetBytes(NomActual.Substring(0, 1))[0] <= 57)) ))
                                        // if ( Encoding.ASCII.GetBytes(datos.Rows[cont]["docoriginal"].ToString().Substring(0,1))[0] <48 || Encoding.ASCII.GetBytes(datos.Rows[cont]["docoriginal"].ToString().Substring(0, 1))[0] > 57)

                                        RenombrarDocumentos("", datos.Rows[cont]["iddocumento"].ToString(),IdUsuario);
                                        Num_Docs++;
                                    }
                                }
                                
                            }
                        }
                    }

            }
            catch (Exception e) { }
        }
        public static string Encripta(string data)
        {
            return "";

        }
    }
    
    




}
