﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Carrefour
{
    public partial class Form1 : Form
    {
        string Ruta = "";
        string IdUsuarioCarrefour = "104";
        string IdUsuarioW2M = "409";
        string IdEmpresaW2M = "18";
        string IdEmpresaCarrefour = "9";
        string IdUsuarioProceso="";

        Int16 NumCaracteresIniciales = 5;
        static WSPro.Service1 WS = new WSPro.Service1();
        public Form1()
        {
            InitializeComponent();
        }
        //*******************************************************************
        private void AltaDatosenFichero()
        { // SUBIDA con un txt creado por ANdres
            try
            {
                string[] directorio = Ruta.Split('\\');

                string IdDocumento = "";

                string Error = "";
                int[,] MatrizContadores = new int[250, 5];
                //Para coger la ruta sin el nombre del Fichero
                string directorio2 = Ruta.Substring(0, Ruta.IndexOf(directorio[directorio.Count() - 1]));

                string FicheroErrores = directorio2 + "Errores" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";
                string FicheroNoEncontrados = directorio2 + "NoEncontrados" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";

                if (!System.IO.Directory.Exists(directorio2 + "\\PROCESADOS"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\PROCESADOS");

                if (!System.IO.Directory.Exists(directorio2 + "\\ERRORES"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\ERRORES");
                string FicheroOrigen = textficheroaltas.Text;
                string[] Fichero = File.ReadAllLines(FicheroOrigen);
                foreach (string Linea in Fichero)
                {
                    string[] Datos = Linea.Split('|');
                    string Dni = Datos[5].Replace("\"","");
                    string Matricula = Datos[3].Replace("\"", "");
                    string RutaDoc = Datos[8].Replace("\"", "");
                    string NomPdf = RutaDoc.Split('\\')[RutaDoc.Split('\\').Count() - 1];
                    int indice = NomPdf.IndexOf(".TIF");
                    NomPdf = NomPdf.Substring(0, indice) + ".pdf";
                    directorio2=directorio2.Replace("\\\\","\\");
                    if (File.Exists(directorio2 + NomPdf))
                    {
                        if (!Funciones.ExisteFicheroPdf(NomPdf, IdUsuarioCarrefour))
                        {
                            String formato = NomPdf.Split('.')[NomPdf.Split('.').Count() - 1];
                            Byte[] PDF = null;
                            PDF = File.ReadAllBytes(directorio2 + NomPdf);

                            string IdExpediente = WS.ExisteExpedienteUsuarioPorNombre(IdUsuarioCarrefour, Dni);
                            if (string.IsNullOrEmpty(IdExpediente)) 
                            {
                                IdExpediente = WS.CrearNuevoExpedienteZDocs(Dni, IdUsuarioCarrefour, "");
                                if (!string.IsNullOrEmpty(IdExpediente))
                                {
                                    Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, IdUsuarioCarrefour);
                                    Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "137");
                                    Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "138");
                                    Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "146");
                                    Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "145");
                                    Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "144");
                                    Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "139");
                                    Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "391");
                                    if (Error != "1")
                                        File.AppendAllText(FicheroErrores, "Error al crear expediente usuario, Idexpediente: " + IdExpediente + Environment.NewLine);
                                }
                                else
                                    File.AppendAllText(FicheroErrores, "Error al crear expediente, Idexpediente: " + IdExpediente + Environment.NewLine);
                            }
                            string DesTipo = Datos[7].Replace("#", "/");
                            DesTipo = DesTipo.Replace("\"", "");
                            string IdTipo = WS.DevolverIdTipo(DesTipo, IdUsuarioCarrefour, "DOCUMENTOS", IdEmpresaCarrefour);
                            if (!string.IsNullOrEmpty(IdTipo))
                                IdDocumento = Funciones.subirFichero(PDF, Matricula + "-" + DesTipo + ".pdf", IdUsuarioCarrefour, IdExpediente, IdEmpresaCarrefour, formato, FicheroErrores);
                            else
                                IdDocumento = Funciones.subirFichero(PDF, NomPdf, IdUsuarioCarrefour, IdExpediente, IdEmpresaCarrefour, formato, FicheroErrores);

                            //LO SUBIMOS COMO INACTIVO HASTA QUE SE REVISE
                            if ( (!checkActivo.Checked) && (IdTipo!="78" && IdTipo!="74" && IdTipo!="105" && IdTipo!="82")  )
                               Error = WS.ActualizarCampo("Documentos", "Activo", "False", " Iddocumento=" + IdDocumento);

                            string ObligadaLectura = "False";
                            if (!WS.ExisteDocumentoUsuario(IdDocumento, "137"))
                            {
                                Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "137", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                if (!string.IsNullOrEmpty(Error))
                                    File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                Error = "";
                            }
                            if (!WS.ExisteDocumentoUsuario(IdDocumento, "138"))
                            {
                                Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "138", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                if (!string.IsNullOrEmpty(Error))
                                    File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                Error = "";
                            }
                            if (!WS.ExisteDocumentoUsuario(IdDocumento, "146"))
                            {
                                Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "146", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                if (!string.IsNullOrEmpty(Error))
                                    File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                Error = "";
                            }
                            if (!WS.ExisteDocumentoUsuario(IdDocumento, "145"))
                            {
                                Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "145", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                if (!string.IsNullOrEmpty(Error))
                                    File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                Error = "";
                            }
                            if (!WS.ExisteDocumentoUsuario(IdDocumento, "144"))
                            {
                                Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "144", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                if (!string.IsNullOrEmpty(Error))
                                    File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                Error = "";
                            }
                            if (!WS.ExisteDocumentoUsuario(IdDocumento, "139"))
                            {
                                Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "139", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                if (!string.IsNullOrEmpty(Error))
                                    File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                Error = "";
                            }
                            if (!WS.ExisteDocumentoUsuario(IdDocumento, "391"))
                            {
                                Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "391", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                if (!string.IsNullOrEmpty(Error))
                                    File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                Error = "";
                            }
                            
                            if (!string.IsNullOrEmpty(IdTipo))
                            {
                                Error = WS.ActualizarDocumentosTipos(IdTipo, IdDocumento, "", "DOCUMENTOS");
                                MatrizContadores[Convert.ToInt16(IdTipo), 0] = Convert.ToInt16(IdTipo);
                                MatrizContadores[Convert.ToInt16(IdTipo), 1]++;

                            }
                            //****** falta este trozo
                            if (!string.IsNullOrEmpty(IdDocumento))
                            {
                                Error=Funciones.ActualizarMetadatosDocumento(IdDocumento, false, true);
                                if (Error == "Matricula no Encontrada")
                                    File.AppendAllText(FicheroNoEncontrados, Linea + Environment.NewLine);
                                Error = WS.ActualizarCampo("Documentos", "NombreOriginal", NomPdf, " Iddocumento=" + IdDocumento);
                                
                                if (Error != "1")
                                    File.AppendAllText(FicheroErrores, Error);
                                
                                Error = "";
                            }
                            else
                                Error = "-1";

                            for (int i = 0; i < 100000; i++)
                                IdDocumento = "";
                            System.IO.Directory.Move(directorio2 + NomPdf, directorio2 + "PROCESADOS\\" + NomPdf);
                        }//Fin del if (!Funciones.ExisteFicheroPdf(NomPdf, IdUsuario))
                        else
                            System.IO.Directory.Move(directorio2 + NomPdf, directorio2 + "ERRORES\\" + NomPdf);
                    }
                    else  // Fin del if (File.Exists(directorio2 + NomPdf))
                        Error = "No existe fichero";
                            
                       
                 } //FIN DEL FOREACH
                string FicherLog = directorio2 + "Log" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";

                int Total = 0;
                int Cont = 1;
                while (Cont < 200)
                {
                    if (MatrizContadores[Cont, 1] != 0)
                    {
                        File.AppendAllText(FicherLog, MatrizContadores[Cont, 1] + " Documentos del Tipo: " + WS.DevolverDescripcionTipo(MatrizContadores[Cont, 0].ToString(), "", IdUsuarioCarrefour) + Environment.NewLine);
                        Total = Total + Convert.ToInt16(MatrizContadores[Cont, 2]);
                    }
                    Cont++;
                }
                File.AppendAllText(FicherLog, Environment.NewLine + " Total:  " + Total);

            }
            catch (Exception e)
            {
            
            }

        }

        //*******************************************************************
        private void SubidaDocumentosRevisionPrevia()
        { // SUBIDA con un txt creado por ANdres, revisamos que concuerde la matricula, antes de subirlos
            try
            {
                string[] directorio = Ruta.Split('\\');

                string IdDocumento = "";

                string Error = "";
                int[,] MatrizContadores = new int[250, 5];
                //Para coger la ruta sin el nombre del Fichero
                string directorio2 = Ruta.Substring(0, Ruta.IndexOf(directorio[directorio.Count() - 1]));

                string FicheroErrores = directorio2 + "Errores" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";
                string FicheroNoEncontrados = directorio2 + "NoEncontrados" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";
                string FicheroPendientes = directorio2 + "\\PENDIENTES\\Pendientes" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";

                if (!System.IO.Directory.Exists(directorio2 + "\\PROCESADOS"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\PROCESADOS");

                if (!System.IO.Directory.Exists(directorio2 + "\\ERRORES"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\ERRORES");
                if (!System.IO.Directory.Exists(directorio2 + "\\PENDIENTES"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\PENDIENTES");

                string FicheroOrigen = textficheroaltas.Text;
                string[] Fichero = File.ReadAllLines(FicheroOrigen);
                foreach (string Linea in Fichero)
                {
                    string[] Datos = Linea.Split('|');
                    string Dni = Datos[5].Replace("\"", "");
                    string Matricula = Datos[3].Replace("\"", "");
                    string RutaDoc = Datos[8].Replace("\"", "");
                    string NomPdf = RutaDoc.Split('\\')[RutaDoc.Split('\\').Count() - 1];
                    int indice = NomPdf.IndexOf(".TIF");
                    NomPdf = NomPdf.Substring(0, indice) + ".pdf";
                 //   directorio2 = directorio2.Replace("\\\\", "\\");
                    if (File.Exists(directorio2 + NomPdf))
                    {
                        if (!Funciones.ExisteFicheroPdf(NomPdf, IdUsuarioCarrefour))
                        {
                            if (Funciones.ExisteMatricula(Matricula))
                            {
                                String formato = NomPdf.Split('.')[NomPdf.Split('.').Count() - 1];
                                Byte[] PDF = null;
                                PDF = File.ReadAllBytes(directorio2 + NomPdf);

                                string IdExpediente = WS.ExisteExpedienteUsuarioPorNombre(IdUsuarioCarrefour, Dni);
                                if (string.IsNullOrEmpty(IdExpediente))
                                {
                                    IdExpediente = WS.CrearNuevoExpedienteZDocs(Dni, IdUsuarioCarrefour, "");
                                    if (!string.IsNullOrEmpty(IdExpediente))
                                    {
                                        Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, IdUsuarioCarrefour);
                                        Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "137");
                                        Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "138");
                                        Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "146");
                                        Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "145");
                                        Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "144");
                                        Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "139");
                                        Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "391");
                                        Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "414");
                                        if (Error != "1")
                                            File.AppendAllText(FicheroErrores, "Error al crear expediente usuario, Idexpediente: " + IdExpediente + Environment.NewLine);
                                    }
                                    else
                                        File.AppendAllText(FicheroErrores, "Error al crear expediente, Idexpediente: " + IdExpediente + Environment.NewLine);
                                }
                                string DesTipo = Datos[7].Replace("#", "/");
                                DesTipo = DesTipo.Replace("\"", "");
                                string IdTipo = WS.DevolverIdTipo(DesTipo, IdUsuarioCarrefour, "DOCUMENTOS", IdEmpresaCarrefour);
                                if (!string.IsNullOrEmpty(IdTipo))
                                    IdDocumento = Funciones.subirFichero(PDF, Matricula + "-" + DesTipo + ".pdf", IdUsuarioCarrefour, IdExpediente, IdEmpresaCarrefour, formato, FicheroErrores);
                                else
                                    IdDocumento = Funciones.subirFichero(PDF, NomPdf, IdUsuarioCarrefour, IdExpediente, IdEmpresaCarrefour, formato, FicheroErrores);

                                //LO SUBIMOS COMO INACTIVO HASTA QUE SE REVISE
                                if ((!checkActivo.Checked) && (IdTipo != "78" && IdTipo != "74" && IdTipo != "105" && IdTipo != "82"))
                                    Error = WS.ActualizarCampo("Documentos", "Activo", "False", " Iddocumento=" + IdDocumento);

                                string ObligadaLectura = "False";
                                if (!WS.ExisteDocumentoUsuario(IdDocumento, "137"))
                                {
                                    Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "137", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                    if (!string.IsNullOrEmpty(Error))
                                        File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                    Error = "";
                                }
                                if (!WS.ExisteDocumentoUsuario(IdDocumento, "138"))
                                {
                                    Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "138", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                    if (!string.IsNullOrEmpty(Error))
                                        File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                    Error = "";
                                }
                                if (!WS.ExisteDocumentoUsuario(IdDocumento, "146"))
                                {
                                    Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "146", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                    if (!string.IsNullOrEmpty(Error))
                                        File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                    Error = "";
                                }
                                if (!WS.ExisteDocumentoUsuario(IdDocumento, "145"))
                                {
                                    Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "145", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                    if (!string.IsNullOrEmpty(Error))
                                        File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                    Error = "";
                                }
                                if (!WS.ExisteDocumentoUsuario(IdDocumento, "144"))
                                {
                                    Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "144", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                    if (!string.IsNullOrEmpty(Error))
                                        File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                    Error = "";
                                }
                                if (!WS.ExisteDocumentoUsuario(IdDocumento, "139"))
                                {
                                    Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "139", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                    if (!string.IsNullOrEmpty(Error))
                                        File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                    Error = "";
                                }
                                if (!WS.ExisteDocumentoUsuario(IdDocumento, "391"))
                                {
                                    Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "391", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                    if (!string.IsNullOrEmpty(Error))
                                        File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                    Error = "";
                                }
                                if (!WS.ExisteDocumentoUsuario(IdDocumento, "414"))
                                {
                                    Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "414", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                    if (!string.IsNullOrEmpty(Error))
                                        File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                    Error = "";
                                }

                                if (!string.IsNullOrEmpty(IdTipo))
                                {
                                    Error = WS.ActualizarDocumentosTipos(IdTipo, IdDocumento, "", "DOCUMENTOS");
                                    MatrizContadores[Convert.ToInt16(IdTipo), 0] = Convert.ToInt16(IdTipo);
                                    MatrizContadores[Convert.ToInt16(IdTipo), 1]++;

                                }
                                //****** falta este trozo
                                if (!string.IsNullOrEmpty(IdDocumento))
                                {
                                    Error = Funciones.ActualizarMetadatosDocumento(IdDocumento, false, true);
                                    if (Error == "Matricula no Encontrada")
                                        File.AppendAllText(FicheroNoEncontrados, Linea + Environment.NewLine);
                                    Error = WS.ActualizarCampo("Documentos", "NombreOriginal", NomPdf, " Iddocumento=" + IdDocumento);

                                    if (Error != "1")
                                        File.AppendAllText(FicheroErrores, Error);

                                    Error = "";
                                }
                                else
                                    Error = "-1";

                                for (int i = 0; i < 100000; i++)
                                    IdDocumento = "";
                                System.IO.Directory.Move(directorio2 + NomPdf, directorio2 + "PROCESADOS\\" + NomPdf);
                            }
                            else
                            { //NO EXISTE LA MATRICULA
                                File.AppendAllText(FicheroPendientes, Linea + Environment.NewLine);
                                System.IO.Directory.Move(directorio2 + NomPdf, directorio2 + "PENDIENTES\\" + NomPdf);
                            }
                        }//Fin del if (!Funciones.ExisteFicheroPdf(NomPdf, IdUsuario))
                        else
                            System.IO.Directory.Move(directorio2 + NomPdf, directorio2 + "ERRORES\\" + NomPdf);
                    }
                    else  // Fin del if (File.Exists(directorio2 + NomPdf))
                        Error = "No existe fichero";


                } //FIN DEL FOREACH
                string FicherLog = directorio2 + "Log" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";

                int Total = 0;
                int Cont = 1;
                while (Cont < 200)
                {
                    if (MatrizContadores[Cont, 1] != 0)
                    {
                        File.AppendAllText(FicherLog, MatrizContadores[Cont, 1] + " Documentos del Tipo: " + WS.DevolverDescripcionTipo(MatrizContadores[Cont, 0].ToString(), "", IdUsuarioCarrefour) + Environment.NewLine);
                        Total = Total + Convert.ToInt16(MatrizContadores[Cont, 2]);
                    }
                    Cont++;
                }
                File.AppendAllText(FicherLog, Environment.NewLine + " Total:  " + Total);


            }
            catch (Exception e)
            {

            }
        }
        private void CorregirTipo()
        {
            try
            {
                string[] directorio = Ruta.Split('\\');

                string IdDocumento = "";

                string Error = "";
                int[,] MatrizContadores = new int[50, 3];
                //Para coger la ruta sin el nombre del Fichero
                string directorio2 = Ruta.Substring(0, Ruta.IndexOf(directorio[directorio.Count() - 1]));

                string FicheroErrores = directorio2 + "Errores" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";

                if (!System.IO.Directory.Exists(directorio2 + "\\PROCESADOS"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\PROCESADOS");

                if (!System.IO.Directory.Exists(directorio2 + "\\ERRORES"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\ERRORES");

                string[] Ficheros = Directory.GetFiles(directorio2);
                foreach (string Fichero in Ficheros)
                {
                    if (Fichero.Substring(Fichero.Length - 4) == ".pdf")
                    {
                        string FicheroPDF = Fichero.Split('\\')[Fichero.Split('\\').Length - 1];
                        String[] aux = FicheroPDF.Split('.');
                        String formato = aux[aux.Length - 1];
                        Byte[] PDF = null;
                        if (File.Exists(directorio2 + FicheroPDF))
                        {
                            PDF = File.ReadAllBytes(directorio2 + FicheroPDF);// + "\\" + Fichero.Substring(Fichero.Length - 4) + ".pdf");                            
                            string Dni = FicheroPDF.Split('-')[0];// Funciones.BuscarDato("DNI", Fichero);
                            string Matricula = FicheroPDF.Split('-')[1];
                            String Tipo = FicheroPDF.Split('-')[2];

                            switch (Tipo)
                            {
                                case "01":
                                    Error = WS.ActualizarDocumentosTipos("73", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 73;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;

                                    break;
                                case "02":
                                    Error = WS.ActualizarDocumentosTipos("2", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 2;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "03":
                                    Error = WS.ActualizarDocumentosTipos("13", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 13;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "04":
                                    Error = WS.ActualizarDocumentosTipos("21", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 21;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "05":
                                    Error = WS.ActualizarDocumentosTipos("26", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 26;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "06":
                                    Error = WS.ActualizarDocumentosTipos("32", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 32;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "07":
                                    Error = WS.ActualizarDocumentosTipos("45", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 45;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "08":
                                    Error = WS.ActualizarDocumentosTipos("56", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 56;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "09":
                                    Error = WS.ActualizarDocumentosTipos("65", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 65;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;

                                case "10":
                                    Error = WS.ActualizarDocumentosTipos("1", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 1;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "11":
                                    Error = WS.ActualizarDocumentosTipos("6", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 6;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "12":
                                    Error = WS.ActualizarDocumentosTipos("8", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 8;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "13":
                                    Error = WS.ActualizarDocumentosTipos("22", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 22;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "14":
                                    Error = WS.ActualizarDocumentosTipos("104", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "15":
                                    Error = WS.ActualizarDocumentosTipos("86", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "16":
                                    Error = WS.ActualizarDocumentosTipos("12", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "17":
                                    Error = WS.ActualizarDocumentosTipos("20", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "18":
                                    Error = WS.ActualizarDocumentosTipos("25", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "19":
                                    Error = WS.ActualizarDocumentosTipos("31", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "20":
                                    Error = WS.ActualizarDocumentosTipos("44", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "21":
                                    Error = WS.ActualizarDocumentosTipos("55", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "22":
                                    Error = WS.ActualizarDocumentosTipos("64", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                                case "23":
                                    Error = WS.ActualizarDocumentosTipos("72", IdDocumento, "", "Documentos");
                                    MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                    MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                    MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                    break;
                            } //swicth
                        }
                    }// Si es un PDF
                }//for each
            } //try
            catch { }
        }

        private void AltaDatosenNombre()
        { // SUBIDA con TIPO PRINCIPAL QUE VIENE EN EL NOMBRE
            try
            {
                string[] directorio = Ruta.Split('\\');

                string IdDocumento = "";

                string Error = "";
                int[,] MatrizContadores = new int[50, 3];
                //Para coger la ruta sin el nombre del Fichero
                string directorio2 = Ruta.Substring(0, Ruta.IndexOf(directorio[directorio.Count() - 1]));

                string FicheroErrores = directorio2 + "Errores" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";
                
                if (!System.IO.Directory.Exists(directorio2 + "\\PROCESADOS"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\PROCESADOS");

                if (!System.IO.Directory.Exists(directorio2 + "\\ERRORES"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\ERRORES");

                string[] Ficheros = Directory.GetFiles(directorio2);
                foreach (string Fichero in Ficheros)
                {
                    if (Fichero.Substring(Fichero.Length - 4) == ".pdf")
                    {
                        string FicheroPDF = Fichero.Split('\\')[Fichero.Split('\\').Length - 1];
                        //string FicheroPDF = FicheroXML.Substring(NumCaracteresIniciales, FicheroXML.Length - NumCaracteresIniciales - 4);// Funciones.BuscarDato("Name", Fichero);
                        //string Tipo = Funciones.BuscarDato("Type", Fichero);
                        String[] aux = FicheroPDF.Split('.');
                        String formato = aux[aux.Length - 1];
                        Byte[] PDF = null;
                        if (File.Exists(directorio2 + FicheroPDF))
                        {
                            PDF = File.ReadAllBytes(directorio2 + FicheroPDF);// + "\\" + Fichero.Substring(Fichero.Length - 4) + ".pdf");                            
                            string Dni = FicheroPDF.Split('-')[0];// Funciones.BuscarDato("DNI", Fichero);
                            string Matricula = FicheroPDF.Split('-')[1];
                            String Tipo = FicheroPDF.Split('-')[2];

                            string IdExpediente = "";
                            if (!string.IsNullOrEmpty(Dni)) 
                            {
                                if (!Funciones.ExisteFicheroPdf(FicheroPDF, IdUsuarioCarrefour))
                                {
                                    IdExpediente = WS.ExisteExpedienteUsuarioPorNombre(IdUsuarioCarrefour, Dni);
                                    if (string.IsNullOrEmpty(IdExpediente))
                                    {
                                        IdExpediente = WS.CrearNuevoExpedienteZDocs(Dni, IdUsuarioCarrefour, "");
                                        if (!string.IsNullOrEmpty(IdExpediente))
                                        {
                                            Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, IdUsuarioCarrefour);
                                            Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "137");
                                            Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "138");
                                            Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "146");
                                            Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "145");
                                            Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "144");
                                            Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "139");
                                            Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "391");
                                            if (Error != "1")
                                                File.AppendAllText(FicheroErrores, "Error al crear expediente usuario, Idexpediente: " + IdExpediente + Environment.NewLine);
                                        }
                                        else
                                            File.AppendAllText(FicheroErrores, "Error al crear expediente, Idexpediente: " + IdExpediente + Environment.NewLine);
                                    }

                                    IdDocumento = Funciones.subirFichero(PDF, FicheroPDF, IdUsuarioCarrefour, IdExpediente, IdEmpresaCarrefour, formato, FicheroErrores);
                                    string ObligadaLectura = "False";
                                    if (!WS.ExisteDocumentoUsuario(IdDocumento, "137"))
                                    {
                                        Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "137", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                        if (!string.IsNullOrEmpty(Error))
                                            File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                        Error = "";
                                    }
                                    if (!WS.ExisteDocumentoUsuario(IdDocumento, "138"))
                                    {
                                        Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "138", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                        if (!string.IsNullOrEmpty(Error))
                                            File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                        Error = "";
                                    }
                                    if (!WS.ExisteDocumentoUsuario(IdDocumento, "146"))
                                    {
                                        Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "146", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                        if (!string.IsNullOrEmpty(Error))
                                            File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                        Error = "";
                                    }
                                    if (!WS.ExisteDocumentoUsuario(IdDocumento, "145"))
                                    {
                                        Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "145", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                        if (!string.IsNullOrEmpty(Error))
                                            File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                        Error = "";
                                    }
                                    if (!WS.ExisteDocumentoUsuario(IdDocumento, "144"))
                                    {
                                        Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "144", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                        if (!string.IsNullOrEmpty(Error))
                                            File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                        Error = "";
                                    }
                                    if (!WS.ExisteDocumentoUsuario(IdDocumento, "139"))
                                    {
                                        Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "139", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                        if (!string.IsNullOrEmpty(Error))
                                            File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                        Error = "";
                                    }
                                    if (!WS.ExisteDocumentoUsuario(IdDocumento, "391"))
                                    {
                                        Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "391", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                        if (!string.IsNullOrEmpty(Error))
                                            File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                        Error = "";
                                    }
                                    switch (Tipo)
                                    {
                                        case "01":
                                            Error=WS.ActualizarDocumentosTipos("73",IdDocumento,"","Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1]=73;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2] ++;
                                            
                                            break;
                                        case "02":
                                            Error = WS.ActualizarDocumentosTipos("2", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 2;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "03":
                                            Error = WS.ActualizarDocumentosTipos("13", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 13;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "04":
                                            Error = WS.ActualizarDocumentosTipos("21", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 21;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "05":
                                            Error = WS.ActualizarDocumentosTipos("26", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 26;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "06":
                                            Error = WS.ActualizarDocumentosTipos("32", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 32;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "07":
                                            Error = WS.ActualizarDocumentosTipos("45", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 45;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "08":
                                            Error = WS.ActualizarDocumentosTipos("56", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 56;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "09":
                                            Error = WS.ActualizarDocumentosTipos("65", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 65;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;

                                        case "10":
                                            Error = WS.ActualizarDocumentosTipos ("1", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 1;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "11":
                                            Error = WS.ActualizarDocumentosTipos("6", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 6;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "12":
                                            Error = WS.ActualizarDocumentosTipos("8", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 8;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "13":
                                            Error = WS.ActualizarDocumentosTipos("22", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 22;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "14":
                                            Error = WS.ActualizarDocumentosTipos("104", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "15":
                                            Error = WS.ActualizarDocumentosTipos("86", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "16":
                                            Error = WS.ActualizarDocumentosTipos("12", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "17":
                                            Error = WS.ActualizarDocumentosTipos("20", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "18":
                                            Error = WS.ActualizarDocumentosTipos("25", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "19":
                                            Error = WS.ActualizarDocumentosTipos("31", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "20":
                                            Error = WS.ActualizarDocumentosTipos("44", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "21":
                                            Error = WS.ActualizarDocumentosTipos("55", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "22":
                                            Error = WS.ActualizarDocumentosTipos("64", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;
                                        case "23":
                                            Error = WS.ActualizarDocumentosTipos("72", IdDocumento, "", "Documentos");
                                            MatrizContadores[Convert.ToInt16(Tipo), 0] = Convert.ToInt16(Tipo);
                                            MatrizContadores[Convert.ToInt16(Tipo), 1] = 104;
                                            MatrizContadores[Convert.ToInt16(Tipo), 2]++;
                                            break;

                                    }
                                    
                                    
                                    if (!string.IsNullOrEmpty(IdDocumento))
                                    {
                                        Funciones.ActualizarMetadatosDocumento(IdDocumento,false,true);
                                        Error = WS.ActualizarCampo("Documentos", "NombreOriginal", FicheroPDF, " Iddocumento=" + IdDocumento);
                                        if (Error != "1")
                                            File.AppendAllText(FicheroErrores, Error);
                                    }
                                    else
                                        Error = "-1";

                                    for (int i = 0; i < 100000; i++)
                                        IdDocumento = "";
                                    System.IO.Directory.Move(directorio2 + FicheroPDF, directorio2 + "PROCESADOS\\" + FicheroPDF);


                                    //System.IO.Directory.Move(directorio2 + FicheroXML, directorio2 + "PROCESADOS\\" + FicheroXML);
                                }
                                else
                                {
                                    System.IO.Directory.Move(directorio2 + FicheroPDF, directorio2 + "ERRORES\\" + FicheroPDF);
                                }

                            }
                        }
                        else
                        {
                            Error = "No existe fichero";
                        }
                    }
                }
                string FicherLog = directorio2 + "Log" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";

                int Total = 0;
                int Cont = 1;
                while (Cont < 50)
                {
                    if (MatrizContadores[Cont, 2] != 0)
                    { 
                        File.AppendAllText(FicherLog, MatrizContadores[Cont, 2] + " Documentos del Tipo: " + WS.DevolverDescripcionTipo(MatrizContadores[Cont, 1].ToString(), "", IdUsuarioCarrefour) + Environment.NewLine);
                        Total = Total + Convert.ToInt16(MatrizContadores[Cont, 2]);
                     }
                    Cont++;
                }
                File.AppendAllText(FicherLog, Environment.NewLine  + " Total:  " + Total);
                MessageBox.Show("Proceso Terminado");
            }
            catch (Exception e)



            {

            }

        }

        private void AltaSubCarpetas()
        {  //SISTEMA DE SUBCARPETAS 001,002...  INCOMPLETO
            try
            {
                string Error = "";
                Ruta =  "F:/Web/CARREFOUR/Carrefour/PDFs IDoc/CAJA08/";
                Ruta = "F:\\Web\\CARREFOUR\\Carrefour\\PDFs IDoc\\CAJA08\\";
                string[] directorio = Ruta.Split('\\');
               // string[] directorio = Ruta.Split('/');
                string Directorio = Ruta.Substring(0, Ruta.IndexOf(directorio[directorio.Count()-1 ]));
                string FicheroErrores = Directorio + "Errores" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";

                if (!System.IO.Directory.Exists(Directorio + "\\PROCESADOS"))
                    System.IO.Directory.CreateDirectory(Directorio + "\\PROCESADOS");

                if (!System.IO.Directory.Exists(Directorio + "\\ERRORES"))
                    System.IO.Directory.CreateDirectory(Directorio + "\\ERRORES");
                
                string[] Ficheros = Directory.GetDirectories(Ruta);
                foreach (string Carpeta in Ficheros)
                {
                    string[] CarpetaActual=Carpeta.Split('\\');
                    string Carpeta2 = CarpetaActual[CarpetaActual.Count() - 1];
                    string Dni = Carpeta2.Split('-')[0];
                    string Matricula = Carpeta2.Split('-')[1];
                    //CREAMOS EL EXPEDINTE SI HACE FALTA
                    string IdExpediente = WS.ExisteExpedienteUsuarioPorNombre(IdUsuarioCarrefour, Dni);
                    if (string.IsNullOrEmpty(IdExpediente))
                    {
                        IdExpediente = WS.CrearNuevoExpedienteZDocs(Dni, IdEmpresaCarrefour, "");
                        if (!string.IsNullOrEmpty(IdExpediente))
                        {
                            Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, IdUsuarioCarrefour);
                            if (Error != "1")
                                File.AppendAllText(FicheroErrores, "Error al crear expediente usuario, Idexpediente: " + IdExpediente);
                        }
                        else
                            File.AppendAllText(FicheroErrores, "Error al crear expediente, Idexpediente: " + IdExpediente);
                    }
                    //ENTRAMOS EN LA CARPETA DEL EXPEDIENTE
                    string[]  CarpetasTipos = Directory.GetDirectories(Carpeta + "\\");
                    foreach (string Tipos in CarpetasTipos)
                    {
                        string[] TiposActual = Tipos.Split('\\');
                        string TipoActual = TiposActual[TiposActual.Count() - 1];
                        switch (TipoActual)
                        {
                            case "001":
                                break;
                            case "002":
                                break;
                        }
                        //Ahora ya cogemos los ficheros de cada carpeta
                        string[] FicherosTipo = Directory.GetFiles(Tipos);
                        //foreach (string Archivo2 in FicherosDni)
                        //{

                        //}
                    }


                }
            }
            catch { }
        }
        
        private void AltaSinMetadatos()
        { // SUBIDA SIN TIPO SOLO CREANDO EL EXPEDIENTE
            try
            {
                string[] directorio = Ruta.Split('\\');
                
                string IdDocumento = "";

                string Error = "";
                //Para coger la ruta sin el nombre del Fichero
                string directorio2 = Ruta.Substring(0, Ruta.IndexOf(directorio[directorio.Count() - 1]));

                string FicheroErrores = directorio2 + "Errores" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";

                if (!System.IO.Directory.Exists(directorio2 + "\\PROCESADOS"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\PROCESADOS");

                if (!System.IO.Directory.Exists(directorio2 + "\\ERRORES"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\ERRORES");

                string[] Ficheros = Directory.GetFiles(directorio2);
                foreach (string Fichero in Ficheros)
                {
                    if (Fichero.Substring(Fichero.Length - 4) == ".pdf")
                    {
                        string FicheroPDF = Fichero.Split('\\')[Fichero.Split('\\').Length - 1];
                        //string FicheroPDF = FicheroXML.Substring(NumCaracteresIniciales, FicheroXML.Length - NumCaracteresIniciales - 4);// Funciones.BuscarDato("Name", Fichero);
                        //string Tipo = Funciones.BuscarDato("Type", Fichero);
                        String[] aux = FicheroPDF.Split('.');
                        String formato = aux[aux.Length - 1];
                        Byte[] PDF = null;
                        if (File.Exists(directorio2 + FicheroPDF))
                        {
                            PDF = File.ReadAllBytes(directorio2 + FicheroPDF);// + "\\" + Fichero.Substring(Fichero.Length - 4) + ".pdf");
                            // string Nombre = Fichero.Substring(0, Fichero.Length - 4).Split('\\')[Fichero.Split('\\').Length - 1];

                            string Dni = FicheroPDF.Split('_')[0];// Funciones.BuscarDato("DNI", Fichero);
                            string Matricula = FicheroPDF.Split('_')[1];

                            string IdExpediente = "";
                            if (!string.IsNullOrEmpty(Dni)) // + ".pdf"))
                            {
                                if (!Funciones.ExisteFicheroPdf(FicheroPDF, IdUsuarioCarrefour))
                                {
                                    IdExpediente = WS.ExisteExpedienteUsuarioPorNombre(IdUsuarioCarrefour, Dni);
                                    if (string.IsNullOrEmpty(IdExpediente))
                                    {
                                        IdExpediente = WS.CrearNuevoExpedienteZDocs(Dni, IdUsuarioCarrefour, "");
                                        if (!string.IsNullOrEmpty(IdExpediente))
                                        {
                                            Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, IdUsuarioCarrefour);
                                            Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "137");
                                            Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "138");
                                            Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "146");
                                            Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "145");
                                            Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, "144");
                                            if (Error != "1")
                                                File.AppendAllText(FicheroErrores, "Error al crear expediente usuario, Idexpediente: " + IdExpediente);
                                        }
                                        else
                                            File.AppendAllText(FicheroErrores, "Error al crear expediente, Idexpediente: " + IdExpediente);
                                    }

                                    IdDocumento = Funciones.subirFichero(PDF, FicheroPDF, IdUsuarioCarrefour, IdExpediente, IdEmpresaCarrefour, formato, FicheroErrores);
                                    string ObligadaLectura = "False";
                                    if (!WS.ExisteDocumentoUsuario(IdDocumento, "137"))
                                    {
                                        Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "137", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                        if (!string.IsNullOrEmpty(Error))
                                            File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                        Error = "";
                                    }
                                    if (!WS.ExisteDocumentoUsuario(IdDocumento, "138"))
                                    {
                                        Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "138", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                        if (!string.IsNullOrEmpty(Error))
                                            File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                        Error = "";
                                    }
                                    if (!WS.ExisteDocumentoUsuario(IdDocumento, "146"))
                                    {
                                        Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "146", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                        if (!string.IsNullOrEmpty(Error))
                                            File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                        Error = "";
                                    }
                                    if (!WS.ExisteDocumentoUsuario(IdDocumento, "145"))
                                    {
                                        Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "145", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                        if (!string.IsNullOrEmpty(Error))
                                            File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                        Error = "";
                                    }
                                    if (!WS.ExisteDocumentoUsuario(IdDocumento, "144"))
                                    {
                                        Error = WS.InsertarDocumentoenDocumentosUsuario(IdDocumento, "144", "Documentos Empresa", IdUsuarioCarrefour, ObligadaLectura, "", "", "", "", IdEmpresaCarrefour);
                                        if (!string.IsNullOrEmpty(Error))
                                            File.AppendAllText(FicheroErrores, "Error insertando el Fichero: " + Fichero + " en Documentos Usuario");
                                        Error = "";
                                    }
                                    if (!string.IsNullOrEmpty(IdDocumento))
                                    {
                                        Error = WS.ActualizarCampo("Documentos", "NombreOriginal", FicheroPDF, " Iddocumento=" + IdDocumento);
                                        if (Error != "1")
                                            File.AppendAllText(FicheroErrores, Error);
                                    }
                                    else
                                        Error = "-1";

                                   
                                    for (int i = 0; i < 155000; i++)
                                        IdDocumento = "";
                                    System.IO.Directory.Move(directorio2 + FicheroPDF, directorio2 + "PROCESADOS\\" + FicheroPDF);


                                    //System.IO.Directory.Move(directorio2 + FicheroXML, directorio2 + "PROCESADOS\\" + FicheroXML);
                                }
                                else
                                {
                                    System.IO.Directory.Move(directorio2 + FicheroPDF, directorio2 + "ERRORES\\" + FicheroPDF);
                                }

                            }
                        }
                        else
                        {
                            Error = "No existe fichero";
                        }
                    }
                }
                MessageBox.Show("Proceso Terminado");
            }
            catch
            {

            }

        }
        private void buttonficheroaltas_Click(object sender, EventArgs e)
        {
            Ficheros.Multiselect = false;
            Ficheros.ShowDialog();

            textficheroaltas.Text = Ficheros.FileName;
            Ruta = Ficheros.FileName;
            // textficheroaltas.Text =  textficheroaltas.Text.ToString().Replace("\\","\\");
            //textficheroaltas.Text = Path.GetFileName(Ficheros.FileName);
        }

        private void button1_Click(object sender, EventArgs e)
        {  //DAR DE ALTA CON LOS XML
            try
            {
                string[] directorio = Ruta.Split('\\');
                string IdUsuario = "104";
                string IdEmpresa = "9";
                string IdDocumento = "";
                
                string Error = "";
                //Para coger la ruta sin el nombre del Fichero
                string directorio2 = Ruta.Substring(0, Ruta.IndexOf(directorio[directorio.Count() - 1]));

                string FicheroErrores = directorio2 + "Errores" + DateTime.Now.ToString().Replace("/","").Replace(":","")  + ".txt";

                if (!System.IO.Directory.Exists(directorio2 + "\\PROCESADOS"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\PROCESADOS");

                if (!System.IO.Directory.Exists(directorio2 + "\\ERRORES"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\ERRORES");

                string[] Ficheros = Directory.GetFiles(directorio2);
                foreach (string Fichero in Ficheros)
                {
                    if (Fichero.Substring(Fichero.Length - 4) == ".xml")
                    {
                        string FicheroXML = Fichero.Split('\\')[Fichero.Split('\\').Length-1];
                        string FicheroPDF = FicheroXML.Substring(NumCaracteresIniciales, FicheroXML.Length -NumCaracteresIniciales- 4);// Funciones.BuscarDato("Name", Fichero);
                        string Tipo = Funciones.BuscarDato("Type", Fichero);
                        String[] aux = FicheroPDF.Split('.');
                        String formato = aux[aux.Length - 1];
                        Byte[] PDF = null; 
                        if (File.Exists( directorio2 + FicheroPDF    ))
                        {
                            PDF = File.ReadAllBytes(directorio2 + FicheroPDF);// + "\\" + Fichero.Substring(Fichero.Length - 4) + ".pdf");
                           // string Nombre = Fichero.Substring(0, Fichero.Length - 4).Split('\\')[Fichero.Split('\\').Length - 1];

                            string Dni = FicheroPDF.Split('_')[0];// Funciones.BuscarDato("DNI", Fichero);
                            string Matricula = FicheroPDF.Split('_')[1];

                            string IdExpediente = "";
                            if  (!string.IsNullOrEmpty(Dni)) // + ".pdf"))
                            {
                                IdExpediente = WS.ExisteExpedienteUsuarioPorNombre(IdUsuario, Dni);
                                if (string.IsNullOrEmpty(IdExpediente))
                                {
                                    IdExpediente = WS.CrearNuevoExpedienteZDocs(Dni, IdUsuario, "");
                                    if (!string.IsNullOrEmpty(IdExpediente))
                                    {
                                        Error = WS.CrearNuevoExpedienteUsuarioZDocs(IdExpediente, IdUsuario);
                                        if (Error != "1")
                                            File.AppendAllText(FicheroErrores, "Error al crear expediente usuario, Idexpediente: " + IdExpediente);
                                    }
                                    else
                                        File.AppendAllText(FicheroErrores, "Error al crear expediente, Idexpediente: " + IdExpediente);
                                }
                                //IdDocumento = Funciones.subirFichero(PDF,  FicheroPDF, IdUsuario, IdExpediente, IdEmpresa);
                                if (Funciones.ValidarCampos(Fichero, IdEmpresa, IdUsuario, Dni, Matricula, Tipo,FicheroErrores) != "-1")
                                {
                                    IdDocumento = Funciones.subirFichero(PDF, Tipo + "-" + Dni + "." + formato, IdUsuario, IdExpediente, IdEmpresa, formato,FicheroErrores);
                                    if (!string.IsNullOrEmpty(IdDocumento))
                                    {
                                        Funciones.LeerCampos(Fichero, IdDocumento, IdEmpresa, IdUsuario, Dni, Matricula, Tipo,FicheroErrores);
                                        Error=WS.ActualizarCampo("Documentos", "NombreOriginal", FicheroPDF, " Iddocumento=" + IdDocumento);
                                        if (Error!="1")
                                            File.AppendAllText(FicheroErrores, Error);
                                    }
                                    
                                    
                                    System.IO.Directory.Move(directorio2 + FicheroPDF, directorio2 + "PROCESADOS\\" + FicheroPDF);
                                    System.IO.Directory.Move(directorio2 + FicheroXML, directorio2 + "PROCESADOS\\" + FicheroXML);
                                    IdDocumento = "";
                                }
                                else
                                {
                                    System.IO.Directory.Move(directorio2 + FicheroPDF, directorio2 + "ERRORES\\" + FicheroPDF);
                                    System.IO.Directory.Move(directorio2 + FicheroXML, directorio2 + "ERRORES\\" + FicheroXML);
                                }

                            }
                        }
                    }
                }
                MessageBox.Show("Proceso Terminado");
            }
            catch
            {
                
            }
                
        }

        private void Form1_Load(object sender, EventArgs e)
        {          
           
           // label1.Text = "Nombre del Expediente";
            button14.Text = "Reasignar Tipo a Documentos";
            comboClientes.Items.Add("Carrefour");
            comboClientes.Items.Add("W2M");

           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult Mensaje = MessageBox.Show("Va a cargar los ficheros de la carpeta seleccionada sin metadatos, sólo creando el Expediente, sin asignarle TIPO, ¿desea continuar?", "Advertencia", MessageBoxButtons.YesNo);
            if (Mensaje == DialogResult.Yes)         
               AltaSinMetadatos();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult Mensaje = MessageBox.Show("Va a cargar los ficheros de la carpeta seleccionada con el sistema sin Xml, sacando los datos del nombre del fichero, ¿desea continuar?", "Advertencia", MessageBoxButtons.YesNo);
            if (Mensaje == DialogResult.Yes  )
                AltaDatosenNombre();
        }

        private void button4_Click(object sender, EventArgs e)
        {

            AltaSubCarpetas();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DialogResult Mensaje = MessageBox.Show("Va a compartir todos los ficheros del usuario carrefour con,  ¿desea continuar?", "Advertencia", MessageBoxButtons.YesNo);
            if (Mensaje == DialogResult.Yes)         
               Funciones.CompartirCon("104", "414", "9");
            MessageBox.Show("Proceso Terminado");
        }

        private void Actualizar()
        {
            try
            {
                switch (comboClientes.Text.ToUpper())
                {
                    case ("W2M"):
                        IdUsuarioProceso = IdUsuarioW2M;

                        break;
                    case "CARREFOUR":
                        IdUsuarioProceso = IdUsuarioCarrefour;
                        break;
                }
                string[] directorio = Ruta.Split('\\');                
                string IdDocumento = "";
                
                string Error = "";
                //Para coger la ruta sin el nombre del Fichero
                string directorio2 = Ruta.Substring(0, Ruta.IndexOf(directorio[directorio.Count() - 1]));

                string FicheroErrores = directorio2 + "Errores" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";
                if (!System.IO.Directory.Exists(directorio2 + "\\PROCESADOS"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\PROCESADOS");

                if (!System.IO.Directory.Exists(directorio2 + "\\ERRORES"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\ERRORES");

                string IdUsuario = IdUsuarioProceso;
                
                string[] Ficheros = Directory.GetFiles(directorio2);
                foreach (string Fichero in Ficheros)
                {
                    string NombreFichero = Fichero.Split('\\')[Fichero.Split('\\').Length - 1];
                    string RutaDoc = "";
                    string NomPdf = Fichero.Split('\\')[Fichero.Split('\\').Count() - 1];
                    IdDocumento = NomPdf.Split('.')[0];
                    //  IdDocumento = Funciones.DevolverIdDocumentodeNombreOriginal(NombreFichero);
                    Byte[] PDF = null; 
                        
                    if (WS.ExisteDocumentoUsuario(IdDocumento,IdUsuarioProceso) && !string.IsNullOrEmpty(IdDocumento))
                        
                    //if (!string.IsNullOrEmpty(IdDocumento))
                    {
                        RutaDoc = WS.DevolverRutaDocumento(IdDocumento);
                        PDF = File.ReadAllBytes(Fichero);
                        Error=WS.ReemplazarFichero(PDF, RutaDoc);
                        if (Error!="1")
                            File.AppendAllText(FicheroErrores, "Error al actualizar el documento, Nombre: " + Fichero);
                        if (!System.IO.File.Exists(directorio2 + "PROCESADOS\\" + NombreFichero))
                            System.IO.File.Move(Fichero, directorio2 + "PROCESADOS\\" + NombreFichero);
                        else
                        {
                            System.IO.File.Delete(directorio2 + "PROCESADOS\\" + NombreFichero);
                            System.IO.File.Move(Fichero, directorio2 + "PROCESADOS\\" + NombreFichero);
                        }
                    }
                    else
                    {
                        System.IO.Directory.Move(Fichero, directorio2 + "ERRORES\\" + NombreFichero);
                        
                        File.AppendAllText(FicheroErrores, "Error No se encuentra el nombre de ese documento, Nombre: " + Fichero);
                    }
                   //PDF = File.ReadAllBytes(directorio2 + FicheroPDF);// + "\\" + Fichero.Substring(Fichero.Length - 4) + ".pdf");
                   

                }
                MessageBox.Show("Proceso Terminado");
            }
            catch
            {

            }

        }

        private void button6_Click(object sender, EventArgs e)
        {
            DialogResult Mensaje = MessageBox.Show("Va a actualizar los ficheros de la carpeta seleccionada, sobreescribiendo la imagen que haya ahora mismo en el servidor, ¿desea continuar?", "Advertencia", MessageBoxButtons.YesNo);
            if (Mensaje == DialogResult.Yes)         
               Actualizar();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            DialogResult Mensaje = MessageBox.Show("Va a compartir los archivos del usuario Carrefour, sólo los que aun no vea el nuevo usuario , ¿desea continuar?", "Advertencia", MessageBoxButtons.YesNo);
            if (Mensaje == DialogResult.Yes)
            {
                //Funciones.ArreglarComparticion("104", "137", "9");
                //Funciones.ArreglarComparticion("104", "138", "9");
                //Funciones.ArreglarComparticion("104", "146", "9");
                Funciones.ArreglarComparticion("104", "144", "9");
                
            }
            
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txttextoactual.Text))
            {
                DialogResult Mensaje = MessageBox.Show("Va a Eliminar el expediente:" + txttextoactual.Text + " y todos sus documentos, ¿Esta seguro?", "Advertencia", MessageBoxButtons.YesNo);
                if (Mensaje == DialogResult.Yes)
                    Funciones.EliminarExpediente(txttextoactual.Text, "104");


                MessageBox.Show("Proceso Finalizado");
            }
            else
                MessageBox.Show("Ha de Introducir un texto descriptivo para el Expediente");
            
            
        }
       

        private void Borrar()
        {
            try
            {
                string[] directorio = Ruta.Split('\\');
                string IdDocumento = "";
               
                //Para coger la ruta sin el nombre del Fichero
                string directorio2 = Ruta.Substring(0, Ruta.IndexOf(directorio[directorio.Count() - 1]));

                string FicheroErrores = directorio2 + "Errores" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";
                if (!System.IO.Directory.Exists(directorio2 + "\\PROCESADOS"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\PROCESADOS");

                if (!System.IO.Directory.Exists(directorio2 + "\\ERRORES"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\ERRORES");


                string[] Ficheros = Directory.GetFiles(directorio2);
                foreach (string Fichero in Ficheros)
                {
                    string NombreFichero = Fichero.Split('\\')[Fichero.Split('\\').Length - 1];                    
                    IdDocumento = Funciones.DevolverIdDocumentodeNombreOriginal(NombreFichero);
                    
                    if (!string.IsNullOrEmpty(IdDocumento))
                    {
                        Funciones.ActualizarCampo("Documentos", "Activo", "False", " iddocumento=" + IdDocumento);
                        
                        System.IO.Directory.Move(Fichero, directorio2 + "PROCESADOS\\" + NombreFichero);
                    }
                    else
                    {
                        System.IO.Directory.Move(Fichero, directorio2 + "ERRORES\\" + NombreFichero);

                        File.AppendAllText(FicheroErrores, "Error No se encuentra el nombre de ese documento, Nombre: " + Fichero);
                    }
                   
                }
                MessageBox.Show("Proceso Terminado");
            }
            catch
            {

            }

        }

        private void button9_Click(object sender, EventArgs e)
        {
            DialogResult Mensaje = MessageBox.Show("Va a Eliminar los documentos del directorio, ¿Esta seguro?", "Advertencia", MessageBoxButtons.YesNo);
            if (Mensaje == DialogResult.Yes)
                Borrar();
            MessageBox.Show("Proceso Finalizado");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            RenomrarTodosDocs();
         //   Funciones.RenombrarDocumentos(txttextoactual.Text,TxtId.Text);
            MessageBox.Show("Proceso Finalizado");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Funciones.RenombrarDocumentosDniEspecifico("0670641F", "00670641F");
            MessageBox.Show("Proceso Finalizado");
        }
        
        private void button12_Click(object sender, EventArgs e)
        {
            //Funciones.ActualizarMetadatos();
            Funciones.ActualizarMetadatosPorExpediente();
            MessageBox.Show("Proceso Finalizado");
        }

        private void button13_Click(object sender, EventArgs e)
        {
            DialogResult Mensaje = MessageBox.Show("Va a Renombrar todos los expedientes que tengan una descripcion de menos de 10 caracteres, ¿Esta seguro?", "Advertencia", MessageBoxButtons.YesNo);
            if (Mensaje == DialogResult.Yes)
                Funciones.RenombrarExpedientes(txttextoactual.Text,txttextoaponer.Text,IdUsuarioCarrefour);
            MessageBox.Show("Proceso Finalizado");
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Funciones.AsignarDocumentos();
            MessageBox.Show("Proceso Finalizado");
        }

        private void btnRenombrarDocs_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(comboClientes.Text))
            {         
                   
                Funciones.RenombrarDocumentosTextoporTexto(txttextoactual.Text, txttextoaponer.Text,Funciones.DevolverUsuario(comboClientes.Text));
            }
            MessageBox.Show("Proceso Finalizado");
        }
        private void RenomrarTodosDocs()
        {
            try
            {
                string IdUsuario = "";
                string IdEmpresa = "";
                string SQL = "";
                if (!string.IsNullOrEmpty(comboClientes.Text))
                    if (comboClientes.Text == "W2M")
                    {
                        IdUsuario = IdUsuarioW2M;
                        IdEmpresa = IdEmpresaW2M;
                        SQL = "Select * from documentos where idusuario=" + IdUsuario + " order by iddocumento";
                        DataTable datos = clases.DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, Funciones.CadenaConexionAnywhereSinProvider);
                        string IdDocumento = "";
                        if (datos.Rows.Count > 0)
                        {
                            for (int cont = 0; cont < datos.Rows.Count; cont++)
                            {
                                IdDocumento = datos.Rows[cont]["Iddocumento"].ToString();
                                string Tipo = WS.DevolverIdTipoDocumento(IdUsuario, IdDocumento);
                                string IdTipoDoc = "";
                                string IdMetadato = "";
                                if (!string.IsNullOrEmpty(Tipo))
                                {
                                    IdTipoDoc = Tipo.Split('|')[0];
                                    IdMetadato = Tipo.Split('|')[1];
                                }
                                if (!string.IsNullOrEmpty(IdTipoDoc) && IdMetadato != "-1")
                                {
                                    string NomTabla = "c00000000";
                                    NomTabla = NomTabla.Substring(0, 9 - IdTipoDoc.Length) + IdTipoDoc;
                                    string NomActual = datos.Rows[cont]["DocOriginal"].ToString();
                                    string NombreNuevo = Funciones.DevolverValorMetadato("Dni", NomTabla, IdDocumento, IdTipoDoc,"") + "-" + WS.DevolverDescripcionTipo(IdTipoDoc, "", IdUsuarioW2M).Substring(3) + "-" + Funciones.DevolverValorMetadato("Fecha", NomTabla, IdDocumento, IdTipoDoc,"yyyymmdd") + "-" + IdDocumento + ".pdf";
                                    if (NomActual != NombreNuevo)
                                        Funciones.ActualizarNombreDocumento(NombreNuevo, IdUsuarioW2M, IdDocumento);
                                }
                            }
                        }

                    }
                    else
                        MessageBox.Show("Debe de tener seleccionado un cliente para poder realizar esta acción");
            }
            catch (Exception e)
            {
            }

        }
        private void btnExistenDocs_Click(object sender, EventArgs e)
        {
            string[] directorio = Ruta.Split('\\');

            //Para coger la ruta sin el nombre del Fichero
            int ContNoPdfs = 0;
            int ContEncontrados = 0;
            int ContNoEncontrados = 0;
            string directorio2 = Ruta.Substring(0, Ruta.IndexOf(directorio[directorio.Count() - 1]));
            string FicheroErrores = directorio2 + "Errores" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";
            string[] Ficheros = Directory.GetFiles(directorio2);
            int Diferencia = 0;
            foreach (string Fichero in Ficheros)
            {
                if (Fichero.Substring(Fichero.Length - 4) == ".pdf")
                {
                    string FicheroPDF = Fichero.Split('\\')[Fichero.Split('\\').Length - 1];

                    if (!Funciones.ExisteFicheroPdf(FicheroPDF, IdUsuarioCarrefour))
                    {
                        ContNoEncontrados++;
                        File.AppendAllText(FicheroErrores, "El Archivo: " + Fichero + " no esta en la BBDD" + Environment.NewLine);
                    }
                    else
                        ContEncontrados++; 

                }
                else
                    ContNoPdfs++;
            }
            if (ContNoEncontrados!=0)
               MessageBox.Show("Hay Archivos que no estan subidos, revise el fichero de Log para ver los detalles");
            else
                MessageBox.Show("Todos los Pdfs de la carpeta esan subidos a Zdocs");
        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(txtIdDocumento.Text))
            //{ 
            if (chk_CmprobarPrimerDigito.Checked)
               Funciones.ActualizarMetadatosDocumento(txtIdDocumento.Text, false,true);
            else
                Funciones.ActualizarMetadatosDocumento(txtIdDocumento.Text, false, false);
            MessageBox.Show("Proceso Terminado");
            //}
            //else
            //    MessageBox.Show("Ha de poner un Id de Documento Primero");
        }

        private void button15_Click(object sender, EventArgs e)
        {            
            Funciones.ActualizarMetadatosDocumento("", true,false);
            MessageBox.Show("Proceso Terminado");
            
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Funciones.ActualizarPorIds(textficheroaltas.Text);
            MessageBox.Show("Proceso Terminado");
        }

        private void button17_Click(object sender, EventArgs e)
        {

            // MONTO EL NOMBRE DE LA TABLA EN DONDE HABRÁ QUE BUSCAR
            string IdTipo = "19";
           // string c = "c00000000";
            string Tabla = "c";
            for (int i = 0; i < 8 - IdTipo.Length; i++)
                Tabla += "0";
            Tabla += IdTipo;

            Boolean  WherePuesto = false;
            Boolean Primero = true;
            string Matriculas = "45654|34234|423423|";
            string Campos = "Campo:Centro;operador:=;Valor:Palma|CAmpo:Dni;Operador:=;Valor:43074456D";
         
            //EJEMPLOS
            string Sql = "select * from [" + Tabla + "]";// inner join tiposdocumento tip on tip.idtipodocumento=" + IdTipo  ;
            if (!string.IsNullOrEmpty(Matriculas))
            {
                if (!WherePuesto)
                {
                    Sql += " where (";
                    WherePuesto = true;
                }
                string[] Matricula = Matriculas.Split('|');
                foreach (string Matr in Matricula)
                {
                    if (!string.IsNullOrEmpty(Matr))
                    {
                        if (!Primero)
                            Sql += " or ";
                        Sql += " Matricula='" + Matr + "'";
                        Primero = false;
                    }
                }
                Sql += ")";
            }
            if (!string.IsNullOrEmpty(Campos))
            {
                if (!WherePuesto)
                {
                    Sql += " where (";
                    WherePuesto = true;
                }
                else
                {
                    Sql += " and "; //NO SE SI PONER AND/OR
                }
                Primero = true;
                string[] Filtros = Campos.Split('|'); 
                //CADA UNO ES UN CAMPO POR EL QUE FILTRAR  
                foreach (string Filtro in Filtros)
                {
                    if (!Primero)
                        Sql += " and ";
                    string[] Dato = Filtro.Split(';'); //SACO LOS TRES CAMPOS DE LA COMPARACION DATO1, OPERADOR Y DATO2
                    Sql += " (" + Dato[0].Split(':')[1] + Dato[1].Split(':')[1] + "'" + Dato[2].Split(':')[1]  + "')";

                    Primero = false;
                }
            }


        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {


        }

        private void btnIncidencias_Click(object sender, EventArgs e)
        {
            string Consulta = "Select * from documentos where tieneincidencia='True' and idusuario=" + IdEmpresaCarrefour;
            DataTable datos = clases.DatabaseConnection.executeNonQueryDT(Consulta, CommandType.Text, Funciones.CadenaConexionAnywhereSinProvider);
            if (datos.Rows.Count > 0)
            {
                for (int i = 0; i < datos.Rows.Count; i++)
                {
                    Funciones.ActualizarMetadatosDocumento(datos.Rows[i]["IdDocumento"].ToString(), false, true);
                 //   Funciones.RenombrarDocumentos("", datos.Rows[i]["IdDocumento"].ToString());
                    Funciones.ActualizarCampo("Documentos", "TieneIncidencia", "False", " IdDocumento=" + datos.Rows[i]["IdDocumento"].ToString());
                }
            }
            MessageBox.Show("Proceso Terminado");

        }

        private void BtnAltaDatosEnFichero_Click(object sender, EventArgs e)
        {
            //AltaDatosenFichero();
            SubidaDocumentosRevisionPrevia();
            MessageBox.Show("Proceso Terminado");
        }

        private void actualizarCentroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Funciones.ActualizarCentroenDocumentos();
            MessageBox.Show("Proceso Terminado");
        }

        private void subidaDeFicherosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 FormSub = new Form2();
            FormSub.Show();
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelaltas_Paint(object sender, PaintEventArgs e)
        {
            

        }

        private void Ficheros_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void administraciónToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void btnBorrarDocs_Click(object sender, EventArgs e)
        {
            Funciones.EliminarDocumentos(Convert.ToDouble(txtfechainicial.Text), Convert.ToDouble(txtfechafinal.Text));
            MessageBox.Show("Proceso Terminado");

        }

        private void btnArreglarComparticionCarrefour_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtIdUsuarioNuevo.Text) && !string.IsNullOrEmpty(txtIdUsuarioPropietario.Text))
            {
                Funciones.ArreglarComparticionCarrefour(txtIdUsuarioPropietario.Text, txtIdUsuarioNuevo.Text, "9");
            }
            MessageBox.Show("Proceso Terminado");
        }

        private void btnActualizarCentroExpedientes_Click(object sender, EventArgs e)
        {
            Funciones.ActualizarCentroenExpedientes();
            MessageBox.Show("Proceso Terminado");
        }
    }
}
