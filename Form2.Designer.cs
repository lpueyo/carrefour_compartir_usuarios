﻿namespace Carrefour
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboCliente = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtId = new System.Windows.Forms.TextBox();
            this.btnRenombrarDocs = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.txttextoaponer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.txttextoactual = new System.Windows.Forms.TextBox();
            this.comboTipo = new System.Windows.Forms.ComboBox();
            this.comboclientes = new System.Windows.Forms.ComboBox();
            this.btnExaminar = new System.Windows.Forms.Button();
            this.Ficheros = new System.Windows.Forms.OpenFileDialog();
            this.TxtRuta = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnprocesar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboCliente
            // 
            this.comboCliente.FormattingEnabled = true;
            this.comboCliente.Location = new System.Drawing.Point(142, 24);
            this.comboCliente.Name = "comboCliente";
            this.comboCliente.Size = new System.Drawing.Size(207, 21);
            this.comboCliente.TabIndex = 92;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 23);
            this.label6.TabIndex = 93;
            // 
            // TxtId
            // 
            this.TxtId.Location = new System.Drawing.Point(0, 0);
            this.TxtId.Name = "TxtId";
            this.TxtId.Size = new System.Drawing.Size(100, 20);
            this.TxtId.TabIndex = 94;
            // 
            // btnRenombrarDocs
            // 
            this.btnRenombrarDocs.Location = new System.Drawing.Point(0, 0);
            this.btnRenombrarDocs.Name = "btnRenombrarDocs";
            this.btnRenombrarDocs.Size = new System.Drawing.Size(75, 23);
            this.btnRenombrarDocs.TabIndex = 95;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(22, 170);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(132, 35);
            this.button13.TabIndex = 79;
            this.button13.Text = "Renombrar Expedientes";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // txttextoaponer
            // 
            this.txttextoaponer.Location = new System.Drawing.Point(0, 0);
            this.txttextoaponer.Name = "txttextoaponer";
            this.txttextoaponer.Size = new System.Drawing.Size(100, 20);
            this.txttextoaponer.TabIndex = 96;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 23);
            this.label5.TabIndex = 97;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 23);
            this.label4.TabIndex = 98;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(0, 0);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 99;
            // 
            // txttextoactual
            // 
            this.txttextoactual.Location = new System.Drawing.Point(0, 0);
            this.txttextoactual.Name = "txttextoactual";
            this.txttextoactual.Size = new System.Drawing.Size(100, 20);
            this.txttextoactual.TabIndex = 100;
            // 
            // comboTipo
            // 
            this.comboTipo.FormattingEnabled = true;
            this.comboTipo.Location = new System.Drawing.Point(151, 115);
            this.comboTipo.Name = "comboTipo";
            this.comboTipo.Size = new System.Drawing.Size(211, 21);
            this.comboTipo.TabIndex = 0;
            // 
            // comboclientes
            // 
            this.comboclientes.FormattingEnabled = true;
            this.comboclientes.Location = new System.Drawing.Point(151, 74);
            this.comboclientes.Name = "comboclientes";
            this.comboclientes.Size = new System.Drawing.Size(211, 21);
            this.comboclientes.TabIndex = 1;
            // 
            // btnExaminar
            // 
            this.btnExaminar.Location = new System.Drawing.Point(438, 29);
            this.btnExaminar.Name = "btnExaminar";
            this.btnExaminar.Size = new System.Drawing.Size(108, 26);
            this.btnExaminar.TabIndex = 2;
            this.btnExaminar.Text = "Examinar";
            this.btnExaminar.UseVisualStyleBackColor = true;
            this.btnExaminar.Click += new System.EventHandler(this.btnExaminar_Click);
            // 
            // Ficheros
            // 
            this.Ficheros.FileName = "Ficheros";
            // 
            // TxtRuta
            // 
            this.TxtRuta.Location = new System.Drawing.Point(151, 33);
            this.TxtRuta.Name = "TxtRuta";
            this.TxtRuta.Size = new System.Drawing.Size(211, 20);
            this.TxtRuta.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Ruta :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Cliente : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tipo de Fichero :";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // btnprocesar
            // 
            this.btnprocesar.Location = new System.Drawing.Point(438, 233);
            this.btnprocesar.Name = "btnprocesar";
            this.btnprocesar.Size = new System.Drawing.Size(108, 26);
            this.btnprocesar.TabIndex = 7;
            this.btnprocesar.Text = "Procesar";
            this.btnprocesar.UseVisualStyleBackColor = true;
            this.btnprocesar.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 317);
            this.Controls.Add(this.btnprocesar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtRuta);
            this.Controls.Add(this.btnExaminar);
            this.Controls.Add(this.comboclientes);
            this.Controls.Add(this.comboTipo);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtId;
        private System.Windows.Forms.Button btnRenombrarDocs;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.TextBox txttextoaponer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.TextBox txttextoactual;
        private System.Windows.Forms.ComboBox comboCliente;
        private System.Windows.Forms.ComboBox comboTipo;
        private System.Windows.Forms.ComboBox comboclientes;
        private System.Windows.Forms.Button btnExaminar;
        private System.Windows.Forms.OpenFileDialog Ficheros;
        private System.Windows.Forms.TextBox TxtRuta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnprocesar;
    }
}