﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.Odbc;

namespace Carrefour
{
    public partial class Form2 : Form
    {
        static WSPro.Service1 WS = new WSPro.Service1();
        //string IdUsuarioW2M = "409";
        //string IdEmpresaW2M = "18";
        //string IdUsuarioCarrefour="104";
        //string IdEmpresaCarrefour = "9";
        string IdUsuarioCarrefour = "104";
        string IdUsuarioW2M = "409";
        string IdEmpresaW2M = "18";
        string IdEmpresaCarrefour = "9";
        string IdUsuarioProceso = "";

        string Ruta = "";
        public Form2()
        {
            InitializeComponent();
        }
        private void RELLENARCOMBOTIPOS()
        {//SOLO PARA EL CLIENTE W2M
            string SQL = "Select descripcion from tipodocumento where idusuario=" + IdUsuarioW2M + " and activo='True' ";
            DataTable datos = clases.DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, Funciones.CadenaConexionAnywhereSinProvider);
            if (datos.Rows.Count > 0)
            {
                foreach (DataRow Fila in datos.Rows)
                {
                    comboTipo.Items.Add(Fila["Descripcion"]);
                }
                

            }
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            comboclientes.Items.Add("W2M");
            comboclientes.Items.Add("EDAM");
            comboclientes.Items.Add("AGUAS DE MARRATXI");
            comboTipo.Items.Add("Tipo1");
            RELLENARCOMBOTIPOS();
            
        }
        private void AltaFicheros()
        {
            try
            {
                string[] directorio = Ruta.Split('\\');
                string IdTipo = "";
                string IdDocumento = "";
                string Error = "";
                int[,] MatrizContadores = new int[250, 5];
                //Para coger la ruta sin el nombre del Fichero
                string directorio2 = Ruta.Substring(0, Ruta.IndexOf(directorio[directorio.Count() - 1]));

                string FicheroErrores = directorio2 + "Errores" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";
                string FicheroNoEncontrados = directorio2 + "NoEncontrados" + DateTime.Now.ToString().Replace("/", "").Replace(":", "") + ".txt";

                if (!System.IO.Directory.Exists(directorio2 + "\\PROCESADOS"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\PROCESADOS");

                if (!System.IO.Directory.Exists(directorio2 + "\\ERRORES"))
                    System.IO.Directory.CreateDirectory(directorio2 + "\\ERRORES");
                string FicheroOrigen = TxtRuta.Text;
                string[] Fichero = File.ReadAllLines(FicheroOrigen);

                string[] drives = System.Environment.GetLogicalDrives();
                DirectoryInfo Directorio = new DirectoryInfo(directorio2);

                foreach (var Fich in Directorio.GetFiles())
                {
                    if (!string.IsNullOrEmpty(comboTipo.Text))
                        IdTipo = WS.DevolverIdTipo(comboTipo.Text, IdUsuarioW2M, "Documentos", IdEmpresaW2M);

                    Byte[] PDF = null;
                    PDF = File.ReadAllBytes(directorio2 + Fich);
                    
                    if (!string.IsNullOrEmpty(IdTipo))
                        IdDocumento = Funciones.subirFichero(PDF, Fich.ToString(), IdUsuarioW2M, "", IdEmpresaW2M, ".pdf", FicheroErrores);
                    else
                        IdDocumento = Funciones.subirFichero(PDF, Fich.ToString(), IdUsuarioW2M, "", IdEmpresaW2M,".pdf", FicheroErrores);

                    if (!string.IsNullOrEmpty(IdTipo))
                    {
                        Error = WS.ActualizarDocumentosTipos(IdTipo, IdDocumento, "", "DOCUMENTOS");
                        MatrizContadores[Convert.ToInt16(IdTipo), 0] = Convert.ToInt16(IdTipo);
                        MatrizContadores[Convert.ToInt16(IdTipo), 1]++;

                    }
                    for (int i = 0; i < 100000; i++)
                        IdDocumento = "";
                    System.IO.Directory.Move(directorio2 + Fich, directorio2 + "PROCESADOS\\" + Fich);
                }
            }
            catch (Exception e)
            { }

        }
        private string SubirFicherosEdam()
        {
            string resul = "";
            return resul;

        }

        private string SubirFicherosAguasMarratxi()
        {
            string resul = "";
            return resul;

        }
        private void btnExaminar_Click(object sender, EventArgs e)
        {
            Ficheros.Multiselect = false;
            Ficheros.ShowDialog();

            TxtRuta.Text = Ficheros.FileName;
            Ruta = Ficheros.FileName;
            // textficheroaltas.Text =  textficheroaltas.Text.ToString().Replace("\\","\\");
            //textficheroaltas.Text = Path.GetFileName(Ficheros.FileName);
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                switch (comboclientes.Text.ToUpper())
                {
                    case ("W2M"):
                        AltaFicheros();

                        break;
                    case "AGUAS DE MARRATXI":
                        break;
                }
             
                MessageBox.Show("Proceso Terminado");
            }
            catch
            { }
        }
        public string CargarExcelMarratxi()
        {
            string CadenaConexionExcel = @"driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};
            driverid=1046;dbq=" + TxtRuta.Text;    //C:\Dev\EXCEL\Plantilla alta fotos.xlsx"; 
            WSPro.Service1 ws = new WSPro.Service1();
            DataSet ds = null;
            int Num_Empresas = 0;
            string IdUsuario, IdEmpresa = "";
            string Incidencias = "";
            string Password = "Zertifika";
            try
            {
                using (OdbcConnection conn = new OdbcConnection(CadenaConexionExcel))
                {
                    conn.Open();
                    using (OdbcCommand command = conn.CreateCommand())
                    {
                        command.CommandText = "SELECT * FROM [Hoja1$]";
                        OdbcDataAdapter ad = new OdbcDataAdapter(command);
                        ds = new DataSet();
                        ad.Fill(ds);
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            var EmailAbonado = dr["EmailAbonado"].ToString();
                            var NombreAbonado = dr["NombreAbonado"].ToString();
                            var Dni = dr["DNIAbonado"].ToString().Replace(" ", "");
                            Dni = Dni.Replace("-", "");
                            var Apellidos = dr["Apellidos"].ToString();

                            if (EmailAbonado != "" && Dni != "")
                            {
                                if (ws.ExisteUsuarioZDocs(EmailAbonado) == "" && ws.ExisteEmpresaZDocs(Dni) == "")
                                {
                                    //InsertarLinea(Consulta)ddd;

                                    IdEmpresa = ws.CrearEmpresaZDocs(NombreAbonado, Dni, "", "", "", "ES", "", EmailAbonado, "", "");
                                    IdUsuario = ws.CrearUsuarioZDocs(NombreAbonado, Apellidos, EmailAbonado, "", Password, Dni, "", "", "", Funciones.Encripta(Password));
                                    ws.CrearUsuarioEmpresaZDocs(IdUsuario, IdEmpresa, true, true);
                                    Funciones.EnviarCorreo(EmailAbonado, "Alta en la Plataforma ZDocs", Funciones.CrearCuerpoCorreoAlta("Aguas de Marratxi", Password), "");

                                    Num_Empresas += 1;
                                }
                            }
                        }
                        
                        Incidencias = "Numero de lineas Procesadas= " + Num_Empresas + ", Incidencias: " + Incidencias;
                        return Incidencias;
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
    }
}
