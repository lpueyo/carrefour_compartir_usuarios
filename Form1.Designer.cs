﻿namespace Carrefour
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label_inf = new System.Windows.Forms.Label();
            this.panelaltas = new System.Windows.Forms.Panel();
            this.btnActualizarCentroExpedientes = new System.Windows.Forms.Button();
            this.btnArreglarComparticionCarrefour = new System.Windows.Forms.Button();
            this.txtIdUsuarioPropietario = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtIdUsuarioNuevo = new System.Windows.Forms.TextBox();
            this.button17 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtId = new System.Windows.Forms.TextBox();
            this.btnRenombrarDocs = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.txttextoaponer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.txttextoactual = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnBorrarDocs = new System.Windows.Forms.Button();
            this.chk_CmprobarPrimerDigito = new System.Windows.Forms.CheckBox();
            this.button15 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtIdDocumento = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtfechainicial = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtfechafinal = new System.Windows.Forms.TextBox();
            this.txtBorarExp = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.comboClientes = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.checkActivo = new System.Windows.Forms.CheckBox();
            this.BtnAltaDatosEnFichero = new System.Windows.Forms.Button();
            this.btnIncidencias = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.btnExistenDocs = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.buttonficheroaltas = new System.Windows.Forms.Button();
            this.textficheroaltas = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Ficheros = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.administraciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarCentroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subidaDeFicherosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelaltas.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_inf
            // 
            this.label_inf.AutoSize = true;
            this.label_inf.BackColor = System.Drawing.Color.Transparent;
            this.label_inf.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_inf.Location = new System.Drawing.Point(847, 46);
            this.label_inf.Name = "label_inf";
            this.label_inf.Size = new System.Drawing.Size(0, 20);
            this.label_inf.TabIndex = 51;
            // 
            // panelaltas
            // 
            this.panelaltas.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelaltas.BackgroundImage")));
            this.panelaltas.Controls.Add(this.btnActualizarCentroExpedientes);
            this.panelaltas.Controls.Add(this.btnArreglarComparticionCarrefour);
            this.panelaltas.Controls.Add(this.txtIdUsuarioPropietario);
            this.panelaltas.Controls.Add(this.label10);
            this.panelaltas.Controls.Add(this.label9);
            this.panelaltas.Controls.Add(this.txtIdUsuarioNuevo);
            this.panelaltas.Controls.Add(this.button17);
            this.panelaltas.Controls.Add(this.panel3);
            this.panelaltas.Controls.Add(this.panel2);
            this.panelaltas.Controls.Add(this.panel1);
            this.panelaltas.Controls.Add(this.button14);
            this.panelaltas.Controls.Add(this.button12);
            this.panelaltas.Controls.Add(this.button11);
            this.panelaltas.Controls.Add(this.button7);
            this.panelaltas.Controls.Add(this.button6);
            this.panelaltas.Controls.Add(this.button5);
            this.panelaltas.Controls.Add(this.button4);
            this.panelaltas.Controls.Add(this.button3);
            this.panelaltas.Controls.Add(this.button1);
            this.panelaltas.Controls.Add(this.label_inf);
            this.panelaltas.Location = new System.Drawing.Point(12, 27);
            this.panelaltas.Name = "panelaltas";
            this.panelaltas.Size = new System.Drawing.Size(1092, 568);
            this.panelaltas.TabIndex = 45;
            this.panelaltas.Paint += new System.Windows.Forms.PaintEventHandler(this.panelaltas_Paint);
            // 
            // btnActualizarCentroExpedientes
            // 
            this.btnActualizarCentroExpedientes.Location = new System.Drawing.Point(921, 303);
            this.btnActualizarCentroExpedientes.Name = "btnActualizarCentroExpedientes";
            this.btnActualizarCentroExpedientes.Size = new System.Drawing.Size(132, 40);
            this.btnActualizarCentroExpedientes.TabIndex = 92;
            this.btnActualizarCentroExpedientes.Text = "Rellenar Centro Todos Expedientes";
            this.btnActualizarCentroExpedientes.UseVisualStyleBackColor = true;
            this.btnActualizarCentroExpedientes.Click += new System.EventHandler(this.btnActualizarCentroExpedientes_Click);
            // 
            // btnArreglarComparticionCarrefour
            // 
            this.btnArreglarComparticionCarrefour.Location = new System.Drawing.Point(921, 238);
            this.btnArreglarComparticionCarrefour.Name = "btnArreglarComparticionCarrefour";
            this.btnArreglarComparticionCarrefour.Size = new System.Drawing.Size(150, 51);
            this.btnArreglarComparticionCarrefour.TabIndex = 91;
            this.btnArreglarComparticionCarrefour.Text = "Arreglar Comparticion Carrefour";
            this.btnArreglarComparticionCarrefour.UseVisualStyleBackColor = true;
            this.btnArreglarComparticionCarrefour.Click += new System.EventHandler(this.btnArreglarComparticionCarrefour_Click);
            // 
            // txtIdUsuarioPropietario
            // 
            this.txtIdUsuarioPropietario.Location = new System.Drawing.Point(838, 280);
            this.txtIdUsuarioPropietario.Name = "txtIdUsuarioPropietario";
            this.txtIdUsuarioPropietario.Size = new System.Drawing.Size(60, 20);
            this.txtIdUsuarioPropietario.TabIndex = 90;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(556, 278);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(256, 20);
            this.label10.TabIndex = 89;
            this.label10.Text = "Id Usuario propietario de los Docs :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(554, 238);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(270, 20);
            this.label9.TabIndex = 88;
            this.label9.Text = "Id Usuario al que vamos a compartir :";
            // 
            // txtIdUsuarioNuevo
            // 
            this.txtIdUsuarioNuevo.Location = new System.Drawing.Point(838, 238);
            this.txtIdUsuarioNuevo.Name = "txtIdUsuarioNuevo";
            this.txtIdUsuarioNuevo.Size = new System.Drawing.Size(60, 20);
            this.txtIdUsuarioNuevo.TabIndex = 87;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(721, 495);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(126, 36);
            this.button17.TabIndex = 86;
            this.button17.Text = "Prueba";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Visible = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.TxtId);
            this.panel3.Controls.Add(this.btnRenombrarDocs);
            this.panel3.Controls.Add(this.button13);
            this.panel3.Controls.Add(this.txttextoaponer);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.button10);
            this.panel3.Controls.Add(this.txttextoactual);
            this.panel3.Location = new System.Drawing.Point(555, 21);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(516, 192);
            this.panel3.TabIndex = 85;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(229, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 20);
            this.label6.TabIndex = 91;
            this.label6.Text = "ID :";
            // 
            // TxtId
            // 
            this.TxtId.Location = new System.Drawing.Point(285, 114);
            this.TxtId.Name = "TxtId";
            this.TxtId.Size = new System.Drawing.Size(58, 20);
            this.TxtId.TabIndex = 90;
            // 
            // btnRenombrarDocs
            // 
            this.btnRenombrarDocs.Location = new System.Drawing.Point(366, 16);
            this.btnRenombrarDocs.Name = "btnRenombrarDocs";
            this.btnRenombrarDocs.Size = new System.Drawing.Size(132, 40);
            this.btnRenombrarDocs.TabIndex = 89;
            this.btnRenombrarDocs.Text = "Renombrar Documento";
            this.btnRenombrarDocs.UseVisualStyleBackColor = true;
            this.btnRenombrarDocs.Click += new System.EventHandler(this.btnRenombrarDocs_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(16, 118);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(132, 35);
            this.button13.TabIndex = 79;
            this.button13.Text = "Renombrar Expedientes";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // txttextoaponer
            // 
            this.txttextoaponer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttextoaponer.Location = new System.Drawing.Point(136, 62);
            this.txttextoaponer.Name = "txttextoaponer";
            this.txttextoaponer.Size = new System.Drawing.Size(207, 22);
            this.txttextoaponer.TabIndex = 65;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 20);
            this.label5.TabIndex = 64;
            this.label5.Text = "Texto a poner:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 20);
            this.label4.TabIndex = 63;
            this.label4.Text = "Texto actual:";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(366, 128);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(132, 51);
            this.button10.TabIndex = 76;
            this.button10.Text = "Renombrar Todos los Documentos del cliente";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // txttextoactual
            // 
            this.txttextoactual.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttextoactual.Location = new System.Drawing.Point(136, 18);
            this.txttextoactual.Name = "txttextoactual";
            this.txttextoactual.Size = new System.Drawing.Size(207, 22);
            this.txttextoactual.TabIndex = 62;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnBorrarDocs);
            this.panel2.Controls.Add(this.chk_CmprobarPrimerDigito);
            this.panel2.Controls.Add(this.button15);
            this.panel2.Controls.Add(this.button8);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.txtIdDocumento);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtfechainicial);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtfechafinal);
            this.panel2.Controls.Add(this.txtBorarExp);
            this.panel2.Location = new System.Drawing.Point(10, 248);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(529, 241);
            this.panel2.TabIndex = 84;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // btnBorrarDocs
            // 
            this.btnBorrarDocs.Location = new System.Drawing.Point(375, 63);
            this.btnBorrarDocs.Name = "btnBorrarDocs";
            this.btnBorrarDocs.Size = new System.Drawing.Size(132, 41);
            this.btnBorrarDocs.TabIndex = 88;
            this.btnBorrarDocs.Text = "Borrar Documentos";
            this.btnBorrarDocs.UseVisualStyleBackColor = true;
            this.btnBorrarDocs.Click += new System.EventHandler(this.btnBorrarDocs_Click);
            // 
            // chk_CmprobarPrimerDigito
            // 
            this.chk_CmprobarPrimerDigito.AutoSize = true;
            this.chk_CmprobarPrimerDigito.Location = new System.Drawing.Point(155, 156);
            this.chk_CmprobarPrimerDigito.Name = "chk_CmprobarPrimerDigito";
            this.chk_CmprobarPrimerDigito.Size = new System.Drawing.Size(139, 17);
            this.chk_CmprobarPrimerDigito.TabIndex = 87;
            this.chk_CmprobarPrimerDigito.Text = "Comprobar Primer Digito";
            this.chk_CmprobarPrimerDigito.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(375, 133);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(132, 40);
            this.button15.TabIndex = 86;
            this.button15.Text = "Rellenar Centro Todos Documentos";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(375, 189);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(126, 36);
            this.button8.TabIndex = 85;
            this.button8.Text = "Rellenar Metadatos Documento";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 187);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 20);
            this.label3.TabIndex = 84;
            this.label3.Text = "Id del Documento:";
            // 
            // txtIdDocumento
            // 
            this.txtIdDocumento.Location = new System.Drawing.Point(166, 189);
            this.txtIdDocumento.Name = "txtIdDocumento";
            this.txtIdDocumento.Size = new System.Drawing.Size(186, 20);
            this.txtIdDocumento.TabIndex = 83;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 20);
            this.label1.TabIndex = 73;
            this.label1.Text = "Fecha Inicial / ID Inicial:";
            // 
            // txtfechainicial
            // 
            this.txtfechainicial.Location = new System.Drawing.Point(194, 18);
            this.txtfechainicial.Name = "txtfechainicial";
            this.txtfechainicial.Size = new System.Drawing.Size(158, 20);
            this.txtfechainicial.TabIndex = 72;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 20);
            this.label2.TabIndex = 82;
            this.label2.Text = "Fecha Final / Id Final:";
            // 
            // txtfechafinal
            // 
            this.txtfechafinal.Location = new System.Drawing.Point(194, 55);
            this.txtfechafinal.Name = "txtfechafinal";
            this.txtfechafinal.Size = new System.Drawing.Size(158, 20);
            this.txtfechafinal.TabIndex = 81;
            // 
            // txtBorarExp
            // 
            this.txtBorarExp.Location = new System.Drawing.Point(375, 9);
            this.txtBorarExp.Name = "txtBorarExp";
            this.txtBorarExp.Size = new System.Drawing.Size(132, 41);
            this.txtBorarExp.TabIndex = 74;
            this.txtBorarExp.Text = "Borrar Expediente";
            this.txtBorarExp.UseVisualStyleBackColor = true;
            this.txtBorarExp.Click += new System.EventHandler(this.button8_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.comboClientes);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.checkActivo);
            this.panel1.Controls.Add(this.BtnAltaDatosEnFichero);
            this.panel1.Controls.Add(this.btnIncidencias);
            this.panel1.Controls.Add(this.button16);
            this.panel1.Controls.Add(this.btnExistenDocs);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.buttonficheroaltas);
            this.panel1.Controls.Add(this.textficheroaltas);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button9);
            this.panel1.Location = new System.Drawing.Point(10, 21);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(529, 221);
            this.panel1.TabIndex = 83;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 20);
            this.label8.TabIndex = 83;
            this.label8.Text = "Cliente :";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // comboClientes
            // 
            this.comboClientes.FormattingEnabled = true;
            this.comboClientes.Location = new System.Drawing.Point(111, 79);
            this.comboClientes.Name = "comboClientes";
            this.comboClientes.Size = new System.Drawing.Size(268, 21);
            this.comboClientes.TabIndex = 82;
            this.comboClientes.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(186, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 20);
            this.label7.TabIndex = 81;
            this.label7.Text = "Cargar Ficheros";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // checkActivo
            // 
            this.checkActivo.AutoSize = true;
            this.checkActivo.Location = new System.Drawing.Point(399, 191);
            this.checkActivo.Name = "checkActivo";
            this.checkActivo.Size = new System.Drawing.Size(114, 17);
            this.checkActivo.TabIndex = 80;
            this.checkActivo.Text = "Documento Activo";
            this.checkActivo.UseVisualStyleBackColor = true;
            // 
            // BtnAltaDatosEnFichero
            // 
            this.BtnAltaDatosEnFichero.Location = new System.Drawing.Point(381, 135);
            this.BtnAltaDatosEnFichero.Name = "BtnAltaDatosEnFichero";
            this.BtnAltaDatosEnFichero.Size = new System.Drawing.Size(132, 33);
            this.BtnAltaDatosEnFichero.TabIndex = 79;
            this.BtnAltaDatosEnFichero.Text = "Datos En Fichero";
            this.BtnAltaDatosEnFichero.UseVisualStyleBackColor = true;
            this.BtnAltaDatosEnFichero.Click += new System.EventHandler(this.BtnAltaDatosEnFichero_Click);
            // 
            // btnIncidencias
            // 
            this.btnIncidencias.Location = new System.Drawing.Point(0, 142);
            this.btnIncidencias.Name = "btnIncidencias";
            this.btnIncidencias.Size = new System.Drawing.Size(75, 38);
            this.btnIncidencias.TabIndex = 78;
            this.btnIncidencias.Text = "Comprobar Incidencias";
            this.btnIncidencias.UseVisualStyleBackColor = true;
            this.btnIncidencias.Visible = false;
            this.btnIncidencias.Click += new System.EventHandler(this.btnIncidencias_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(72, 139);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(79, 40);
            this.button16.TabIndex = 77;
            this.button16.Text = "Reprocesar IDs";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Visible = false;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // btnExistenDocs
            // 
            this.btnExistenDocs.Location = new System.Drawing.Point(0, 112);
            this.btnExistenDocs.Name = "btnExistenDocs";
            this.btnExistenDocs.Size = new System.Drawing.Size(78, 39);
            this.btnExistenDocs.TabIndex = 76;
            this.btnExistenDocs.Text = "Existen Documentos";
            this.btnExistenDocs.UseVisualStyleBackColor = true;
            this.btnExistenDocs.Visible = false;
            this.btnExistenDocs.Click += new System.EventHandler(this.btnExistenDocs_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(9, 46);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(66, 20);
            this.label15.TabIndex = 63;
            this.label15.Text = "Fichero:";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // buttonficheroaltas
            // 
            this.buttonficheroaltas.Location = new System.Drawing.Point(421, 46);
            this.buttonficheroaltas.Name = "buttonficheroaltas";
            this.buttonficheroaltas.Size = new System.Drawing.Size(86, 25);
            this.buttonficheroaltas.TabIndex = 61;
            this.buttonficheroaltas.Text = "Examinar";
            this.buttonficheroaltas.UseVisualStyleBackColor = true;
            this.buttonficheroaltas.Click += new System.EventHandler(this.buttonficheroaltas_Click);
            // 
            // textficheroaltas
            // 
            this.textficheroaltas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textficheroaltas.Location = new System.Drawing.Point(111, 46);
            this.textficheroaltas.Name = "textficheroaltas";
            this.textficheroaltas.Size = new System.Drawing.Size(268, 22);
            this.textficheroaltas.TabIndex = 62;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(72, 177);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(124, 42);
            this.button2.TabIndex = 67;
            this.button2.Text = "Nuevo Sistema,Datos en Nombre";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(0, 179);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 39);
            this.button9.TabIndex = 75;
            this.button9.Text = "Borrar Documentos";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Visible = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(571, 331);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(158, 51);
            this.button14.TabIndex = 80;
            this.button14.Text = "Reasignar Tipo a Documentos";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Visible = false;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(10, 495);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(170, 51);
            this.button12.TabIndex = 78;
            this.button12.Text = "Actualizar Metadatos Todos los Documentos";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Visible = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(571, 420);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(158, 53);
            this.button11.TabIndex = 77;
            this.button11.Text = "Renombrar Documentos Dni Especifico";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Visible = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(897, 497);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(156, 51);
            this.button7.TabIndex = 71;
            this.button7.Text = "Arreglar Comparticion";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Visible = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(913, 440);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(158, 51);
            this.button6.TabIndex = 70;
            this.button6.Text = "Actualizar";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Visible = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(571, 497);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(126, 34);
            this.button5.TabIndex = 69;
            this.button5.Text = "Compartir";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(913, 381);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(158, 53);
            this.button4.TabIndex = 68;
            this.button4.Text = "Subir Subcarpetas-I+Doc";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(735, 372);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(158, 49);
            this.button3.TabIndex = 66;
            this.button3.Text = "Cargar Sin Metadatos";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(391, 497);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 36);
            this.button1.TabIndex = 64;
            this.button1.Text = "Cargar desde Xml";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Ficheros
            // 
            this.Ficheros.FileName = "Ficheros";
            this.Ficheros.FileOk += new System.ComponentModel.CancelEventHandler(this.Ficheros_FileOk);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.administraciónToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1116, 24);
            this.menuStrip1.TabIndex = 46;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // administraciónToolStripMenuItem
            // 
            this.administraciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actualizarCentroToolStripMenuItem,
            this.subidaDeFicherosToolStripMenuItem});
            this.administraciónToolStripMenuItem.Name = "administraciónToolStripMenuItem";
            this.administraciónToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
            this.administraciónToolStripMenuItem.Text = "Administración";
            this.administraciónToolStripMenuItem.Click += new System.EventHandler(this.administraciónToolStripMenuItem_Click);
            // 
            // actualizarCentroToolStripMenuItem
            // 
            this.actualizarCentroToolStripMenuItem.Name = "actualizarCentroToolStripMenuItem";
            this.actualizarCentroToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.actualizarCentroToolStripMenuItem.Text = "Actualizar Centro";
            this.actualizarCentroToolStripMenuItem.Click += new System.EventHandler(this.actualizarCentroToolStripMenuItem_Click);
            // 
            // subidaDeFicherosToolStripMenuItem
            // 
            this.subidaDeFicherosToolStripMenuItem.Name = "subidaDeFicherosToolStripMenuItem";
            this.subidaDeFicherosToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.subidaDeFicherosToolStripMenuItem.Text = "Subida de Ficheros";
            this.subidaDeFicherosToolStripMenuItem.Click += new System.EventHandler(this.subidaDeFicherosToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1116, 622);
            this.Controls.Add(this.panelaltas);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelaltas.ResumeLayout(false);
            this.panelaltas.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_inf;
        private System.Windows.Forms.Panel panelaltas;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textficheroaltas;
        private System.Windows.Forms.Button buttonficheroaltas;
        private System.Windows.Forms.OpenFileDialog Ficheros;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtfechainicial;
        private System.Windows.Forms.Button txtBorarExp;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtfechafinal;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txttextoaponer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txttextoactual;
        private System.Windows.Forms.Button btnRenombrarDocs;
        private System.Windows.Forms.Button btnExistenDocs;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtIdDocumento;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtId;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.CheckBox chk_CmprobarPrimerDigito;
        private System.Windows.Forms.Button btnIncidencias;
        private System.Windows.Forms.Button BtnAltaDatosEnFichero;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem administraciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarCentroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subidaDeFicherosToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkActivo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboClientes;
        private System.Windows.Forms.Button btnBorrarDocs;
        private System.Windows.Forms.Button btnArreglarComparticionCarrefour;
        private System.Windows.Forms.TextBox txtIdUsuarioPropietario;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtIdUsuarioNuevo;
        private System.Windows.Forms.Button btnActualizarCentroExpedientes;
    }
}

